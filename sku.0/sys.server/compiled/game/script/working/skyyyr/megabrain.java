package script.working.skyyyr;

import script.*;
import script.base_class.*;
import script.library.*;
import script.base_script;
import java.util.StringTokenizer;

public class megabrain extends script.base_script
{
    public int OnSpeaking(obj_id self, String txt)
    {
        StringTokenizer st = new StringTokenizer(txt);
        obj_id target = utils.stringToObjId(st.nextToken());
        if (target == null || target == obj_id.NULL_ID)
        {
            target = self;
        }
        String arg = st.nextToken();
        if (arg.equals(""))
        {
            return SCRIPT_CONTINUE;
        }
        if (arg.equals("-help"))
        {
            sendSystemMessageTestingOnly(self, "Command List:");
            sendSystemMessageTestingOnly(self, "All commands require the prefix - and all commands camel case. They are case sensitive. If you would like more information about each command, type -<commandName> help. EXAMPLE: -age help");
            sendSystemMessageTestingOnly(self, "age, cureCloningSickness, setGlowy, playEffect, playMusic, playerWarp, damage, creature, canSee, buff, revert");
        }
        else if (arg.equals("-age"))
        {
            String help = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (help.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] Put your mouse over the target you wish to get the age of.");
            }
            else
            {
                int age = getCurrentBirthDate() - getPlayerBirthDate(target);
                sendSystemMessageTestingOnly(self, "Target: " + getName(target) + "'s age is: " + age + ".");
                sendSystemMessageTestingOnly(target, "Your characters age is: " + age);
            }
        }
        else if (arg.equals("-cureCloningSickness") || arg.equals("-ccs"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP](-ccs) -cureCloningSickness (lookAtTarget)|| -cureCloningSickness <area> (64m range)");
            }
            else if (param.equals("area"))
            {
                obj_id[] pir = getPlayerCreaturesInRange(self, 64);
                for (int intI = 0; intI < pir.length; intI++)
                {
                    if (isIdValid(pir[intI]) && isPlayer(pir[intI]))
                    {
                        playClientEffectObj(pir[intI], "appearance/pt_cure_cloning_sickness.prt", pir[intI], "");
                        buff.removeBuff(pir[intI], "cloning_sickness");
                        playMusic(pir[intI], "sound/vo_meddroid_01.snd");
                        sendSystemMessageTestingOnly(self, "[EVENT CURE] Target: " + getName(pir[intI]) + " has been cured of cloning sickness.");
                        sendSystemMessageTestingOnly(target, "[EVENT CURE] " + getName(self) + " has cured your cloning sickness.");
                    }
                }
            }
            else
            {
                playClientEffectObj(target, "appearance/pt_cure_cloning_sickness.prt", target, "");
                buff.removeBuff(target, "cloning_sickness");
                playMusic(target, "sound/vo_meddroid_01.snd");
                sendSystemMessageTestingOnly(self, "Target: " + getName(target) + " has been cured of cloning sickness.");
                sendSystemMessageTestingOnly(target, getName(self) + " has cured your cloning sickness.");
            }
        }
        else if (arg.equals("-setGlowy"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] Put your mouse over the target you wish to change glowy for.");
            }
            if (getState(target, STATE_GLOWING_JEDI) == 0)
            {
                setState(target, STATE_GLOWING_JEDI, true);
                sendSystemMessageTestingOnly(self, "[GLOWY] Target: " + getName(target) + " has been set to glowy.");
                sendSystemMessageTestingOnly(target, "[GLOWY] " + getName(self) + " has connected you to the Force!");
            }
            else
            {
                setState(target, STATE_GLOWING_JEDI, false);
                sendSystemMessageTestingOnly(self, "[GLOWY] Target: " + getName(target) + " has been taken out of glowy.");
                sendSystemMessageTestingOnly(target, "[GLOWY] " + getName(self) + " has removed you from the Force connection!");
            }
        }
        else if (arg.equals("-playEffect"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] -playEffect <here> pt_cure_cloning_sickness || -playEffect pt_cure_cloning_sickness || -playEffect <area> pt_cure_cloning_sickness");
            }
            else if (param.equals("here"))
            {
                String effect = st.nextToken();
                playClientEffectLoc(self, "appearance/" + effect + ".prt", getLocation(self), 0.0f);
                sendSystemMessageTestingOnly(self, "You have played a client effect at location: " + getLocation(self));
            }
            else if (param.equals("area"))
            {
                obj_id[] pir = getPlayerCreaturesInRange(self, 64);
                for (int intI = 0; intI < pir.length; intI++)
                {
                    if (isIdValid(pir[intI]) && isPlayer(pir[intI]))
                    {
                        String effect = st.nextToken();
                        playClientEffectLoc(self, "appearance/" + effect + ".prt", getLocation(pir[intI]), 0.0f);
                        sendSystemMessageTestingOnly(self, "[EFFECT] You have played a client effect on: " + getName(pir[intI]));
                    }
                }
            }
            else
            {
                playClientEffectObj(target, "appearance/" + param + ".prt", target, "");
                sendSystemMessageTestingOnly(self, "[EFFECT] You have played a client effect on " + getName(target));
            }
            
        }
        else if (arg.equals("-playMusic"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] -playMusic <nameOfSong> (self)|| -playMusic <area> (64m range)|| -playMusic <target> (name or look at target)");
                sendSystemMessageTestingOnly(self, "[HELP] sound/<putSongNameHere>.snd");
            }
            else if (param.equals("area"))
            {
                obj_id[] pir = getPlayerCreaturesInRange(self, 64);
                for (int intI = 0; intI < pir.length; intI++)
                {
                    if (isIdValid(pir[intI]) && isPlayer(pir[intI]))
                    {
                        playMusic(pir[intI], "sound/" + param + ".snd");
                        sendSystemMessageTestingOnly(self, "[MUSIC] You have played " + getName(pir[intI]) + " a song.");
                    }
                }
            }
            else if (param.equals("target"))
            {
                playMusic(target, "sound/" + param + ".snd");
                sendSystemMessageTestingOnly(self, "[MUSIC] You have played " + getName(target) + " a song.");
            }
            else
            {
                playMusic(self, "sound/" + param + ".snd");
                sendSystemMessageTestingOnly(self, "[MUSIC] You have played " + getName(self) + " a song.");
            }
        }
        else if (arg.equals("-damage"))
        {
            String param = st.nextToken();
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] -damage area range damageAmount");
                sendSystemMessageTestingOnly(self, "[HELP] -damage target targetName damageAmount");
            }
            else if (param.equals("area"))
            {
                String range = st.nextToken();
                int range1 = utils.stringToInt(range);
                String damageAmount = st.nextToken();
                int damAmount = utils.stringToInt(damageAmount);
                obj_id[] objObjects = getPlayerCreaturesInRange(self, range1);
                for (int intI = 0; intI < objObjects.length; intI++)
                {
                    if ((isIdValid(objObjects[intI]) && isPlayer(objObjects[intI])))
                    {
                        damage(objObjects[intI], DAMAGE_ELEMENTAL_HEAT, HIT_LOCATION_BODY, damAmount);
                        sendSystemMessageTestingOnly(self,"[DAMAGE] Name: " + getName(objObjects[intI]) + ", Damage Amount: " + damageAmount + ", range: " + range1 + ".");
                    }
                    else
                    {
                        sendSystemMessageTestingOnly(self, "[DAMAGE] No player targets within range.");
                        return SCRIPT_CONTINUE;
                    }
                }
            }
            else if (param.equals("target"))
            {
                obj_id tmp = utils.stringToObjId(st.nextToken());
                String damageAmount = st.nextToken();
                int damAmount = utils.stringToInt(damageAmount);
                if (isIdValid(tmp))
                {
                    target = tmp;
                }
                damage(target, DAMAGE_ELEMENTAL_HEAT, HIT_LOCATION_BODY, damAmount);
                sendSystemMessageTestingOnly(self,"[DAMAGE] Name: " + getName(target) + ", Damage Amount: " + damageAmount + ".");
            }
        }
        else if (arg.equals("-creature"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] -creature aiCommand");
                sendSystemMessageTestingOnly(self, "[HELP] Here is a list of all aiCommands. Info about each group catagory will lead with [] to help you identify each command.");
                sendSystemMessageTestingOnly(self, "[HELP] [MOVEMENT] stop, loiter, flee, [POSTURE] stand, kneel, prone, knockdown, [LOCOMOTION] standing, walk, run, [SPEED] fast, slow, [BITMASK] invul, vul");
            }
            else if (param.equals("stop"))
            {
                stop(target);
                sendSystemMessageTestingOnly(self, "[CREATURE AI] " + getName(target) + " has been stopped.");
            }
            else if (param.equals("loiter"))
            {
                loiterTarget(target, self, 0, 5, 0, 0);
                sendSystemMessageTestingOnly(self, "[CREATURE AI] " + getName(target) + " has been set to loiter.");
            }
            else if (param.equals("flee"))
            {
                flee(target, self, 5, 10);
                sendSystemMessageTestingOnly(self, "[CREATURE AI] " + getName(target) + " has been set to flee.");
            }
            else if (param.equals("stand"))
            {
                setPosture(target, POSTURE_UPRIGHT);
                sendSystemMessageTestingOnly(self, "[CREATURE POSTURE] " + getName(target) + " has been set to stand.");
            }
            else if (param.equals("kneel"))
            {
                setPosture(target, POSTURE_CROUCHED);
                sendSystemMessageTestingOnly(self, "[CREATURE POSTURE] " + getName(target) + " has been set to kneel.");
            }
            else if (param.equals("prone"))
            {
                setPosture(target, POSTURE_PRONE);
                sendSystemMessageTestingOnly(self, "[CREATURE POSTURE] " + getName(target) + " has been set to prone.");
            }
            else if (param.equals("knockdown"))
            {
                setPosture(target, POSTURE_LYING_DOWN);
                sendSystemMessageTestingOnly(self, "[CREATURE POSTURE] " + getName(target) + " has been set to knockdown.");
            }
            else if (param.equals("standing"))
            {
                setPosture(target, LOCOMOTION_STANDING);
                sendSystemMessageTestingOnly(self, "[CREATURE LOCOMOTION] " + getName(target) + " has been set to standing.");
            }
            else if (param.equals("walk"))
            {
                setLocomotion(target, LOCOMOTION_WALKING);
                sendSystemMessageTestingOnly(self, "[CREATURE LOCOMOTION] " + getName(target) + " has been set to walking.");
            }
            else if (param.equals("run"))
            {
                setLocomotion(target, LOCOMOTION_RUNNING);
                sendSystemMessageTestingOnly(self, "[CREATURE LOCOMOTION] " + getName(target) + " has been set to running.");
            }
            else if (param.equals("fast"))
            {
                setMovementRun(target);
                sendSystemMessageTestingOnly(self, "[CREATURE SPEED] " + getName(target) + " has been set to fast.");
            }
            else if (param.equals("slow"))
            {
                setMovementWalk(target);
                sendSystemMessageTestingOnly(self, "[CREATURE SPEED] " + getName(target) + " has been set to slow.");
            }
            else if (param.equals("invul"))
            {
                setInvulnerable(target, true);
                sendSystemMessageTestingOnly(self, "[CREATURE SPEED] " + getName(target) + " has been set to invulnerable.");
            }
            else if (param.equals("vul"))
            {
                setInvulnerable(target, false);
                sendSystemMessageTestingOnly(self, "[CREATURE SPEED] " + getName(target) + " has been set to vulnerable.");
            }
        }
        else if (arg.equals("-canSee"))
        {
            target = utils.stringToObjId(st.nextToken());
            if (canSee(target, self))
            {
                sendSystemMessageTestingOnly(self, "[ADMIN] " + getName(target) + " can be seen by you.");
            }
            else
            {
                sendSystemMessageTestingOnly(self, "[ADMIN] " + getName(target) + " CANNOT be seen by you.");
            }
        }
        else if (arg.equals("-buff"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] -buff || -buff area");
            }
            else if (param.equals("area"))
            {
                obj_id[] pir = getPlayerCreaturesInRange(self, 64);
                for (int intI = 0; intI < pir.length; intI++)
                {
                    if ((isIdValid(pir[intI]) && isPlayer(pir[intI])))
                    {
                        buff.applyBuff(pir[intI], "buildabuff_inspiration", 3600);
                        addSkillModModifier(pir[intI], "buildabuff_expertise_action_all", "expertise_action_all", 9, 3600, false, true);
                        addSkillModModifier(pir[intI], "buildabuff_expertise_glancing_blow_all", "expertise_glancing_blow_all", 7, 3600, false, true);
                        addSkillModModifier(pir[intI], "buildabuff_expertise_innate_protection_energy", "expertise_innate_protection_energy", 3750, 3600, false, true);
                        addSkillModModifier(pir[intI], "buildabuff_expertise_innate_protection_kinetic", "expertise_innate_protection_kinetic", 3750, 3600, false, true);
                        buff.applyBuff(pir[intI], "me_buff_health_2", 3600);
                        buff.applyBuff(pir[intI], "me_buff_action_3", 3600);
                        buff.applyBuff(pir[intI], "me_buff_strength_3", 3600);
                        buff.applyBuff(pir[intI], "me_buff_agility_3", 3600);
                        buff.applyBuff(pir[intI], "me_buff_precision_3", 3600);
                        buff.applyBuff(pir[intI], "me_buff_ranged_gb_1", 3600);
                        buff.applyBuff(pir[intI], "me_buff_melee_gb_1", 3600); 
                        buff.applyBuff(pir[intI], "drink_flameout", 3600);
                        sendSystemMessageTestingOnly(self,"[EVENT BUFF] Name: " + getName(pir[intI]));
                        sendSystemMessageTestingOnly(pir[intI],"[EVENT BUFF] " + getName(self) + " buffed you for: ac 9, gb 7, en 3750, kin 3750, all meds, and flameout.");
                    }
                    else
                    {
                        sendSystemMessageTestingOnly(self, "[EVENT BUFF] No players within range.");
                        return SCRIPT_CONTINUE;
                    }
                }
            }
            else if (param.equals("custom"))
            {
                String custom = st.nextToken();
                buff.applyBuff(target, custom, 3600);
                sendSystemMessageTestingOnly(self, "[EVENT BUFF] Target: " + getName(target)+ " has been buffed with buff: " + custom + ", for 1 hour.");
            }
            else
            {
                buff.applyBuff(target, "buildabuff_inspiration", 3600);
                addSkillModModifier(target, "buildabuff_expertise_action_all", "expertise_action_all", 9, 3600, false, true);
                addSkillModModifier(target, "buildabuff_expertise_glancing_blow_all", "expertise_glancing_blow_all", 7, 3600, false, true);
                addSkillModModifier(target, "buildabuff_expertise_innate_protection_energy", "expertise_innate_protection_energy", 3750, 3600, false, true);
                addSkillModModifier(target, "buildabuff_expertise_innate_protection_kinetic", "expertise_innate_protection_kinetic", 3750, 3600, false, true);
                buff.applyBuff(target, "me_buff_health_2", 3600);
                buff.applyBuff(target, "me_buff_action_3", 3600);
                buff.applyBuff(target, "me_buff_strength_3", 3600);
                buff.applyBuff(target, "me_buff_agility_3", 3600);
                buff.applyBuff(target, "me_buff_precision_3", 3600);
                buff.applyBuff(target, "me_buff_ranged_gb_1", 3600);
                buff.applyBuff(target, "me_buff_melee_gb_1", 3600);
                buff.applyBuff(target, "drink_flameout", 3600);
                sendSystemMessageTestingOnly(self,"[EVENT BUFF] Name: " + getName(target));
                sendSystemMessageTestingOnly(target,"[EVENT BUFF] " + getName(self) + " buffed you for: ac 9, gb 7, en 3750, kin 3750, all meds, and flameout.");
            }
        }
        else if (arg.equals("-revert"))
        {
            String param = st.nextToken();
            obj_id tmp = utils.stringToObjId(st.nextToken());
            if (isIdValid(tmp))
            {
                target = tmp;
            }
            if (param.equals("help"))
            {
                sendSystemMessageTestingOnly(self, "[HELP] This target will revert your targets appearance back to normal.");
            }
            revertObjectAppearance(target);
        }
        return SCRIPT_CONTINUE;
    }
}

/*
removeTriggerVolume("");
createTriggerVolume(VOLUME_NAME, 15.0f, true);
public int OnObjectDamaged(obj_id self, obj_id attacker, obj_id weapon, int damage)
    {
        int curHP = getHitpoints(self);
        int maxHP = getMaxHitpoints(self);
        int smolder = (maxHP - (maxHP / 3));
        int fire = (maxHP / 3);
        if (!hasObjVar(self, "playingEffect"))
        {
            if (curHP < smolder)
            {
                if (curHP < fire)
                {
                    location death = getLocation(self);
                    setObjVar(self, "playingEffect", 1);
                    messageTo(self, "effectManager", null, 15, true);
                }
                else 
                {
                    location death = getLocation(self);
                    setObjVar(self, "playingEffect", 1);
                    messageTo(self, "effectManager", null, 15, true);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
*/