/*
 Title:        modifiable_string_id
 Description:  Modifiable extension of a string_id.
 */

package script;

public class modifiable_string_id extends string_id
{
	public modifiable_string_id(String table, int id) {
		super("", "");
	}

	/**
	 * Class constructor.
	 *
	 * @param table		the string table
	 * @param id		the string table id
	 */
	public modifiable_string_id(String table, String id)
	{
		super(table, id);
	}	// modifiable_string_id(String, String)

	/**
	 * Copy constructor.
	 *
	 * @param src		class instance to copy
	 */
	public modifiable_string_id(string_id src)
	{
		super(src);
	}	// modifiable_string_id(string_id)

	/**
	 * Sets the table for the string id.
	 *
	 * @param table		the table name
	 */
	public void setTable(String table)
	{
		this.table = table;
	}	// setTable
}	// class modifiable_string_id
