/*
 Title:        attribute
 Description:  Wrapper for an attribute.
 */

package script;

import java.io.Serializable;

public final class attribute implements Comparable, Serializable
{
	private final static long serialVersionUID = 2234290627397819837L;

	private final int type;
	private final int value;

	/*
	 * Class constructor.
	 *
	 * @param type		attribute type
	 * @param value		attribute value
	 */
	public attribute(int type, int value)
	{
		this.type = type;
		this.value = value;
	}	// attribute

	 /*
	  * Copy constructor.
	  *
	  * @param src		class instance to copy
	  */
	 public attribute(attribute src)
	 {
		type = src.type;
		value = src.value;
	 }	// attribute(attribute)

	/*
	 * Accessor function.
	 *
	 * @return the attribute type
	 */
	public int getType()
	{
		return type;
	}	// gettype

	/*
	 * Accessor function.
	 *
	 * @return the attribute value
	 */
	public int getValue()
	{
		return value;
	}	// getValue

	/*
	 * Conversion function.
	 *
	 * @return the attribute as a string.
	 */
	public String toString()
	{
		return "(type=" + Integer.toString(type) +
			", value=" + Integer.toString(value) +
			")";
	}	// toString

	/*
	 * Compares this to a generic object.
	 *
	 * @returns <, =, or > 0 if the object is an attribute, else throws
	 *		ClassCastException
	 */
	public int compareTo(Object o) throws ClassCastException
	{
		return compareTo((attribute)o);
	}	// compareTo(Object)

	/*
	 * Compares this to another attribute.
	 *
	 * @returns <, =, or > 0
	 */
	public int compareTo(attribute id)
	{
		if (type == id.type)
			return value - id.value;
		return type - id.type;
	}	// compareTo(attribute)

	/*
	 * Compares this to a generic object.
	 *
	 * @returns true if the objects have the same data, false if not
	 */
	public boolean equals(Object o)
	{
		try
		{
			int result = compareTo(o);
			if (result == 0)
				return true;
		}
		catch (ClassCastException err)
		{
		}
		return false;
	}	// equals

}	// class attribute
