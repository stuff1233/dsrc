package script.space_mining;

import script.*;

import script.library.space_content;

public class asteroid extends script.base_script
{
    public asteroid()
    {
    }
    public int OnAttach(obj_id self)
    {
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        return SCRIPT_CONTINUE;
    }
    public int OnDestroy(obj_id self)
    {
        if (hasObjVar(self, "objParent"))
        {
            space_content.notifySpawner(self);
        }
        return SCRIPT_CONTINUE;
    }
}
