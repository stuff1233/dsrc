package script.npc.boss;

import script.*;

import script.library.buff;

public class boss_movement_buff extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        buff.applyBuff(self, "boss_armor_break_immunity");
        buff.applyBuff(self, "boss_snare_immunity");
        buff.applyBuff(self, "boss_root_immunity");
        buff.applyBuff(self, "boss_mez_immunity");
        buff.applyBuff(self, "boss_slow_immunity");
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        buff.applyBuff(self, "boss_armor_break_immunity");
        buff.applyBuff(self, "boss_snare_immunity");
        buff.applyBuff(self, "boss_root_immunity");
        buff.applyBuff(self, "boss_mez_immunity");
        buff.applyBuff(self, "boss_slow_immunity");
        return SCRIPT_CONTINUE;
    }
}
