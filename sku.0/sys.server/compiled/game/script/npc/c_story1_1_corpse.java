package script.npc;

import script.*;

public class c_story1_1_corpse extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        if (!isMob(self) || isPlayer(self))
        {
            detachScript(self, "npc.c_story1_1_corpse");
        }
        setInvulnerable(self, true);
        setName(self, "Moxxar Krieg");
        setAnimationMood(self, "npc_dead_03");
        return SCRIPT_CONTINUE;
    }
    public int OnAttach(obj_id self)
    {
        setInvulnerable(self, true);
        setName(self, "Moxxar Krieg");
        setAnimationMood(self, "npc_dead_03");
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info menuInfo)
    {
        int menu = menuInfo.addRootMenu(menu_info_types.EXAMINE, null);
        menu_info_data menuInfoData = menuInfo.getMenuItemById(menu);
        menuInfoData.setServerNotify(false);
        return SCRIPT_CONTINUE;
    }
    public int OnIncapacitated(obj_id self, obj_id killer)
    {
        detachScript(self, "conversation.c_story1_1_corpse");
        return SCRIPT_CONTINUE;
    }
}
