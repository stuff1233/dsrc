package script.systems.gcw;

import script.*;

import script.library.factions;
import script.library.utils;

public class gcw_city_kit_barricade extends script.systems.gcw.gcw_city_kit
{
    public void setupConstructionQuests(obj_id self, obj_id pylon)
    {
        setName(pylon, "Barricade Construction Site");
        utils.setScriptVar(pylon, "gcw.name", "Barricade Construction Site");
        attachScript(pylon, "systems.gcw.gcw_city_pylon_barricade");
    }
    public obj_id createFactionKit(int faction, location loc)
    {
        if (loc == null)
        {
            return null;
        }
        obj_id kit = null;
        if (factions.FACTION_FLAG_IMPERIAL == faction)
        {
            kit = createObject("object/tangible/destructible/gcw_imperial_barricade.iff", loc);
            setName(kit, "Imperial Barricade");
        }
        else if (factions.FACTION_FLAG_REBEL == faction)
        {
            kit = createObject("object/tangible/destructible/gcw_rebel_barricade.iff", loc);
            setName(kit, "Rebel Barricade");
        }
        return kit;
    }
}
