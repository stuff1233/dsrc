package script.systems.storyteller.events;

import script.base_class.*;
import script.base_script;
import script.dictionary;
import script.obj_id;

import script.library.ai_lib;
import script.library.holiday;

public class anniversary_event_nyms extends script.base_script
{
    public anniversary_event_nyms()
    {
    }
    public int OnInitialize(obj_id self)
    {
        if (!isObjectPersisted(self))
        {
            messageTo(self, "handlePersistEventProp", null, 1, false);
        }
        if (holiday.ACTIVE_HOLIDAY == 0)
            messageTo(self, "handleDeleteEventProps", null, 2, false);
        return SCRIPT_CONTINUE;
    }
    public int OnAttach(obj_id self)
    {
        messageTo(self, "handlePersistEventProp", null, 1, false);
        return SCRIPT_CONTINUE;
    }
    public int handlePersistEventProp(obj_id self, dictionary params)
    {
        persistObject(self);
        return SCRIPT_CONTINUE;
    }
    public int handleDeleteEventProps(obj_id self, dictionary params)
    {
        destroyObject(self);
        return SCRIPT_CONTINUE;
    }
}
