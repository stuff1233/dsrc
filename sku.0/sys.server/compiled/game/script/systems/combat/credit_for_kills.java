package script.systems.combat;

import script.*;

import java.util.ArrayList;
import java.util.List;

import script.library.xp;
import script.library.bounty_hunter;

public class credit_for_kills extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        xp.setupCreditForKills();
        return SCRIPT_CONTINUE;
    }
    public int OnDetach(obj_id self)
    {
        xp.cleanupCreditForKills();
        return SCRIPT_CONTINUE;
    }
    public int OnDeath(obj_id self, obj_id killer, obj_id corpseId)
    {
        if (self == null) {
            return SCRIPT_CONTINUE;
        }
        xp.assessCombatResults(self);
        messageTo(self, xp.HANDLER_XP_DELEGATED, null, 0.1f, false);
        xp.applyHealingCredit(self);
        obj_id attacker = getObjIdObjVar(self, xp.VAR_TOP_GROUP);
        if (hasObjVar(self, "bountyCheckPayBracket"))
        {
            bounty_hunter.payBountyCheckReward(attacker, self);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectDisabled(obj_id self, obj_id killer)
    {
        xp.assessCombatResults(self);
        messageTo(self, xp.HANDLER_XP_DELEGATED, null, 0.1f, false);
        xp.applyHealingCredit(self);
        return SCRIPT_CONTINUE;
    }
    public int addEnemyHealer(obj_id self, dictionary params)
    {
        if (params != null)
        {
            obj_id healer = params.getObjId("healer");
            if (isIdValid(healer))
            {
                deltadictionary scriptVars = self.getScriptVars();
                List<obj_id> healers = scriptVars.getResizeableObjIdArray("healers");
                if (healers == null)
                {
                    healers = new ArrayList<>();
                }
                if (!healers.contains(healer))
                {
                    healers.add(healer);
                    scriptVars.put("healers", healers);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
}
