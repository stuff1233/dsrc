package script.systems.combat;

import script.*;

public class combat_actions_humanoid extends script.base_script
{
    public combat_actions_humanoid()
    {
    }
    public int OnLogin(obj_id self)
    {
        if (!hasScript(self, "systems.combat.combat_actions"))
        {
            attachScript(self, "systems.combat.combat_actions");
        }
        detachScript(self, "systems.combat.combat_actions_humanoid");
        return SCRIPT_CONTINUE;
    }
}
