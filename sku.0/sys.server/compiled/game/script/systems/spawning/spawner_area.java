package script.systems.spawning;

import script.dictionary;
import script.library.ai_lib;
import script.library.create;
import script.library.spawning;
import script.library.utils;
import script.location;
import script.obj_id;

public class spawner_area extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        if (!hasObjVar(self, "registerWithController"))
        {
            setObjVar(self, "registerWithController", 1);
        }
        doSpawnEvent(self, null);
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        if (!hasObjVar(self, "registerWithController"))
        {
            setObjVar(self, "registerWithController", 1);
        }
        doSpawnEvent(self, null);
        return SCRIPT_CONTINUE;
    }
    public void doSpawnEvent(obj_id self, dictionary params)
    {
        if (spawning.checkSpawnCount(self))
        {
            float fltRadius = getFloatObjVar(self, "fltRadius");
            String strNamesStr = getStringObjVar(self, "strName");
            if (strNamesStr == null) {
                return;
            }
            String[] strNames = strNamesStr.split(":");
            //float fltSize = 2.0f;
            location locTest = spawning.getRandomLocationInCircle(getLocation(self), fltRadius);
            if (getIntObjVar(self, "intGoodLocationSpawner") > 0) {
                for (String strName : strNames) {
                    requestLocation(self, strName, locTest, rand(100, 200), (float)getIntObjVar(self, "intSpawnCount"), true, true);
                }
            } else {
                for (String strName : strNames) {
                    createMob(strName, null, locTest, fltRadius, self);
                }
            }
        }
    }
    public float getClosestSize(float fltOriginalSize)
    {
        if (fltOriginalSize <= 4.0f)
        {
            return 4.0f;
        }
        if (fltOriginalSize <= 8.0f)
        {
            return 8.0f;
        }
        if (fltOriginalSize <= 12.0f)
        {
            return 12.0f;
        }
        if (fltOriginalSize <= 16.0f)
        {
            return 16.0f;
        }
        if (fltOriginalSize <= 32.0f)
        {
            return 32.0f;
        }
        else if (fltOriginalSize <= 48.0f)
        {
            return 48.0f;
        }
        else if (fltOriginalSize <= 64.0f)
        {
            return 64.0f;
        }
        else if (fltOriginalSize <= 80.0f)
        {
            return 80f;
        }
        else if (fltOriginalSize <= 96.0f)
        {
            return 96f;
        }
        return 32f;
    }
    private float getRespawnTime(obj_id self){
        if (hasObjVar(self, "fltRespawnTime")) {
            return getFloatObjVar(self, "fltRespawnTime");
        }
        return rand(getFloatObjVar(self, "fltMinSpawnTime"), getFloatObjVar(self, "fltMaxSpawnTime"));
    }
    public void createMob(String strId, obj_id objLocationObject, location locLocation, float fltRadius, obj_id self)
    {
        if (!spawning.checkSpawnCount(self))
        {
            return;
        }
        if (isIdValid(objLocationObject))
        {
            destroyObject(objLocationObject);
        }
        obj_id objTemplate;
        if (strId.endsWith(".iff"))
        {
            objTemplate = createObject(strId, locLocation);
            if (!isIdValid(objTemplate))
            {
                setName(self, "BAD MOB OF TYPE " + strId);
                return;
            }
        }
        else 
        {
            objTemplate = create.object(strId, locLocation);
            if (!isIdValid(objTemplate))
            {
                System.out.println("Bad mob name: " + strId + " (" + getStringObjVar(self, "strSpawns") + ")");
                setName(self, "BAD MOB OF TYPE " + strId);
                return;
            }
            int intBehavior = getIntObjVar(self, "intDefaultBehavior");
            ai_lib.setDefaultCalmBehavior(objTemplate, intBehavior);
        }
        float fltRespawnTime = getRespawnTime(self);
        spawning.incrementSpawnCount(self);
        spawning.addToSpawnDebugList(self, objTemplate);
        setObjVar(objTemplate, "objParent", self);
        setObjVar(objTemplate, "fltRespawnTime", fltRespawnTime);
        attachScript(objTemplate, "systems.spawning.spawned_tracker");
        setYaw(objTemplate, getYaw(self));

        if (!spawning.checkSpawnCount(self))
        {
            return;
        }
        messageTo(self, "doSpawnEvent", null, fltRespawnTime, false);
    }
    public int OnLocationReceived(obj_id self, String strId, obj_id objLocationObject, location locLocation, float fltRadius)
    {
        if (isIdValid(objLocationObject))
        {
            createMob(strId, objLocationObject, locLocation, fltRadius, self);
        }
        else 
        {
            messageTo(self, "doSpawnEvent", null, getRespawnTime(self), false);
        }
        return SCRIPT_CONTINUE;
    }
    public int spawnDestroyed(obj_id self, dictionary params)
    {
        int intCurrentSpawnCount = utils.getIntScriptVar(self, "intCurrentSpawnCount");
        intCurrentSpawnCount--;
        if (intCurrentSpawnCount >= 0)
        {
            utils.setScriptVar(self, "intCurrentSpawnCount", intCurrentSpawnCount);
        }
        else 
        {
            utils.setScriptVar(self, "intCurrentSpawnCount", 0);
        }
        messageTo(self, "doSpawnEvent", null, 2, false);
        return SCRIPT_CONTINUE;
    }
    public int OnDestroy(obj_id self)
    {
        if (utils.hasScriptVar(self, "debugSpawnList"))
        {
            obj_id[] spawns = utils.getObjIdArrayScriptVar(self, "debugSpawnList");
            if (spawns == null || spawns.length == 0)
            {
                return SCRIPT_CONTINUE;
            }
            for (obj_id spawn : spawns) {
                if (isIdValid(spawn) && exists(spawn)) {
                    messageTo(spawn, "selfDestruct", null, 5, false);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
}
