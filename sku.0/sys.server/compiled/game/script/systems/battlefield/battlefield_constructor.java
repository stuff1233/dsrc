package script.systems.battlefield;

import script.*;

import script.library.battlefield;

public class battlefield_constructor extends script.base_script
{
    public static final string_id SID_BUILD_STRUCTURE = new string_id("battlefield", "build_structure");
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi)
    {
        if (battlefield.isSameBattlefieldFaction(player, self))
        {
            mi.addRootMenu(menu_info_types.SERVER_TRAVEL_OPTIONS, SID_BUILD_STRUCTURE);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item)
    {
        if (item == menu_info_types.SERVER_TRAVEL_OPTIONS)
        {
            queueCommand(player, (1271146555), null, "", COMMAND_PRIORITY_DEFAULT);
        }
        return SCRIPT_CONTINUE;
    }
}
