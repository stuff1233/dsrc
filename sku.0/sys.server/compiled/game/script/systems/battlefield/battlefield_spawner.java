package script.systems.battlefield;

import script.*;

import script.library.battlefield;

public class battlefield_spawner extends script.base_script
{
    public static final String VAR_NUMBER_SPAWNS = "battlefield.number_spawns";
    public int OnInitialize(obj_id self)
    {
        setObjVar(self, battlefield.VAR_SPAWNER_CURRENT_POPULATION, 0);
        setObjVar(self, battlefield.VAR_SPAWNER_CURRENT, 0);
        int pulse = getIntObjVar(self, battlefield.VAR_SPAWNER_PULSE);
        messageTo(self, "msgBattlefieldSpawn", null, pulse, false);
        return SCRIPT_CONTINUE;
    }
    public int msgBattlefieldSpawn(obj_id self, dictionary params)
    {
        int num_spawns = 0;
        if (hasObjVar(self, VAR_NUMBER_SPAWNS))
        {
            num_spawns = getIntObjVar(self, VAR_NUMBER_SPAWNS);
        }
        int max_spawns = getIntObjVar(self, battlefield.VAR_SPAWNER_MAX);
        int pulse = getIntObjVar(self, battlefield.VAR_SPAWNER_PULSE);
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        battlefield.createRandomSpawn(master_object, self);
        num_spawns--;
        if (num_spawns < max_spawns)
        {
            messageTo(self, "msgBattlefieldSpawn", null, pulse, false);
        }
        return SCRIPT_CONTINUE;
    }
}
