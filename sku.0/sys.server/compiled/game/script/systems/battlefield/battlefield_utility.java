package script.systems.battlefield;

import script.*;

import script.library.battlefield;
import script.library.factions;
import script.library.utils;

public class battlefield_utility extends script.base_script
{
    public int OnSpeaking(obj_id self, String text)
    {
        location loc = getLocation(self);
        if (text.startsWith("bfdestroy"))
        {
            obj_id target = getLookAtTarget(self);
            if (target != null)
            {
                loc = getLocation(target);
            }
            else 
            {
                java.util.StringTokenizer st = new java.util.StringTokenizer(text);
                if (st.hasMoreTokens())
                {
                    String target_str = st.nextToken();
                    target = utils.stringToObjId(target_str);
                    if (target != null)
                    {
                        loc = getLocation(target);
                    }
                }
            }
            battlefield.destroyBattlefield(target);
        }
        if (text.startsWith("endbattle"))
        {
            obj_id target = getLookAtTarget(self);
            if (target == null || target == obj_id.NULL_ID)
            {
                java.util.StringTokenizer st = new java.util.StringTokenizer(text);
                if (st.hasMoreTokens())
                {
                    String target_str = st.nextToken();
                    target = utils.stringToObjId(target_str);
                }
            }
            if (target != null && target != obj_id.NULL_ID)
            {
                battlefield.endBattlefield(target);
                sendSystemMessageTestingOnly(self, "Battle ended.");
            }
        }
        if (text.equals("battlefield"))
        {
            String area = getCurrentSceneName();
            battlefield.createBattlefieldRegions(area);
            sendSystemMessageTestingOnly(self, "Battlefields created.");
        }
        if (text.equals("destroyall"))
        {
            String area = getCurrentSceneName();
            battlefield.destroyBattlefieldRegions(area);
            sendSystemMessageTestingOnly(self, "Battlefields destroyed.");
        }
        if (text.startsWith("delta"))
        {
            obj_id target = obj_id.NULL_ID;
            java.util.StringTokenizer st = new java.util.StringTokenizer(text);
            if (st.hasMoreTokens())
            {
                String target_str = st.nextToken();
                target = utils.stringToObjId(target_str);
            }
            if (target == null)
            {
                target = getLookAtTarget(self);
            }
            if (target != null && target != obj_id.NULL_ID)
            {
                location targ_loc = getLocation(target);
                float delta_x = targ_loc.getX() - loc.getX();
                float delta_y = targ_loc.getY() - loc.getY();
                float delta_z = targ_loc.getZ() - loc.getZ();
                sendSystemMessageTestingOnly(self, "(" + delta_x + ", " + delta_y + ", " + delta_z + ")");
            }
        }
        if (text.equals("clearbattleobjs"))
        {
            obj_id target = getLookAtTarget(self);
            if (target != null && target != obj_id.NULL_ID)
            {
                if (hasObjVar(target, battlefield.VAR_BATTLEFIELD))
                {
                    removeObjVar(target, battlefield.VAR_BATTLEFIELD);
                }
            }
            else 
            {
                if (hasObjVar(self, battlefield.VAR_BATTLEFIELD))
                {
                    removeObjVar(self, battlefield.VAR_BATTLEFIELD);
                }
            }
        }
        if (text.equals("nuke"))
        {
            obj_id[] objects = getObjectsInRange(loc, 20.0f);
            for (int i = 0; i < objects.length; i++)
            {
                if (!isPlayer(objects[i]))
                {
                    destroyObject(objects[i]);
                }
            }
        }
        if (text.equals("meganuke"))
        {
            obj_id[] objects = getObjectsInRange(loc, 250.0f);
            for (int i = 0; i < objects.length; i++)
            {
                if (!isPlayer(objects[i]))
                {
                    destroyObject(objects[i]);
                }
            }
        }
        if (text.equals("faction"))
        {
            obj_id target = getLookAtTarget(self);
            if (target != null)
            {
                region bf = battlefield.getBattlefield(target);
                int faction_id = pvpBattlefieldGetFaction(target, bf);
                String faction = factions.getFactionNameByHashCode(faction_id);
                sendSystemMessageTestingOnly(self, "faction ->" + faction);
                factions.addFactionStanding(self, faction, 250.0f);
            }
            else 
            {
                int faction_id = pvpGetAlignedFaction(self);
                String faction = factions.getFactionNameByHashCode(faction_id);
                sendSystemMessageTestingOnly(self, "faction ->" + faction);
                factions.addFactionStanding(self, faction, 250.0f);
            }
        }
        if (text.startsWith("warp"))
        {
            java.util.StringTokenizer st = new java.util.StringTokenizer(text);
            if (st.hasMoreTokens())
            {
                String planet = st.nextToken();
                float x = (float)utils.stringToInt(st.nextToken());
                float y = (float)utils.stringToInt(st.nextToken());
                float z = (float)utils.stringToInt(st.nextToken());
                warpPlayer(self, planet, x, y, z, null, 0.0f, 0.0f, 0.0f);
            }
        }
        if (text.equals("bfobject"))
        {
            region bf = battlefield.getBattlefield(self);
            if (bf != null)
            {
                obj_id bf_object = battlefield.getMasterObjectFromRegion(bf);
                sendSystemMessageTestingOnly(self, "bf_object ->" + bf_object);
            }
        }
        if (text.startsWith("getposture"))
        {
            obj_id target = obj_id.NULL_ID;
            java.util.StringTokenizer st = new java.util.StringTokenizer(text);
            if (st.hasMoreTokens())
            {
                String target_str = st.nextToken();
                target = utils.stringToObjId(target_str);
            }
            if (isIdValid(target))
            {
                sendSystemMessageTestingOnly(self, "posture ->" + getPosture(target));
            }
        }
        return SCRIPT_CONTINUE;
    }
}
