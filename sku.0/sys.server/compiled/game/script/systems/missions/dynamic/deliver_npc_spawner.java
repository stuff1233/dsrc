package script.systems.missions.dynamic;

import script.*;

import script.library.create;
import script.library.holiday;
import script.library.locations;

public class deliver_npc_spawner extends script.base_script {
    public int OnAttach(obj_id self) {
        if (holiday.ACTIVE_HOLIDAY == 4) {
            location here = getLocation(self);
            String city = locations.getCityName(here);
            if (city == null) {
                city = locations.getGuardSpawnerRegionName(here);
            }
            if (city != null && city.length() > 0) {
                if (city.equals("theed")) {
                    return SCRIPT_CONTINUE;
                }
            }
        }
        location here = getLocation(self);
        obj_id npc = create.object("commoner", here);
        attachScript(npc, "systems.missions.dynamic.mission_deliver_npc");
        return SCRIPT_CONTINUE;
    }
}
