package script.library;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import script.base_script;

import script.library.regions;
import script.library.trial;

public class locations extends script.base_script
{
    public locations()
    {
    }
    public static final float GOOD_LOCATION_SEARCH_SIZE = 200;
    public static final String[] PLANETS = 
    {
        "tatooine",
        "naboo",
        "talus",
        "corellia",
        "yavin4",
        "dantooine",
        "dathomir",
        "lok",
        "rori",
        "endor"
    };
    public static final String NO_AREA = "no_area";
    public static final String PLANET_LEVEL_TABLE = "datatables/spawning/planetary_data/planet_level.iff";
    public static float getRegionExtents(region rgnTest)
    {
        location[] locExtents = getRegionExtent(rgnTest);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        float fltXDistance = locUpperRight.getX() - locLowerLeft.getX();
        float fltZDistance = locUpperRight.getZ() - locLowerLeft.getZ();
        if (fltXDistance > fltZDistance)
        {
            LOG("mission", "fltXDistance returning with a value of " + fltXDistance);
            return fltXDistance;
        }
        else 
        {
            LOG("mission", "fltZDistance returning with a value of " + fltZDistance);
            return fltZDistance;
        }
    }
    public static region getSmallestRegion(region[] rgnRegions)
    {
        if (rgnRegions == null)
        {
            return null;
        }
        if (rgnRegions.length == 0)
        {
            return null;
        }
        region rgnSmallestRegion = rgnRegions[0];
        float fltExtents = 1000000000;
        for (int intI = 0; intI < rgnRegions.length; intI++)
        {
            float fltRegionExtents = getRegionExtents(rgnRegions[intI]);
            if (fltRegionExtents < fltExtents)
            {
                rgnSmallestRegion = rgnRegions[intI];
                fltExtents = fltRegionExtents;
            }
        }
        return rgnSmallestRegion;
    }
    public static float getRegionXSize(region rgnSearchRegion)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        float fltXDistance = locUpperRight.getX() - locLowerLeft.getX();
        return fltXDistance;
    }
    public static float getRegionZSize(region rgnSearchRegion)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        float fltZDistance = locUpperRight.getZ() - locLowerLeft.getZ();
        return fltZDistance;
    }
    public static float[] getRegionSize(region rgnSearchRegion)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        float fltXDistance = locUpperRight.getX() - locLowerLeft.getX();
        float fltZDistance = locUpperRight.getZ() - locLowerLeft.getZ();
        float[] fltRegionSize = new float[2];
        fltRegionSize[0] = fltXDistance;
        fltRegionSize[1] = fltZDistance;
        return fltRegionSize;
    }
    public static location getRegionCenter(region rgnSearchRegion)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        location locRegionCenter = new location(locLowerLeft);
        locRegionCenter.setX((locLowerLeft.getX() + locUpperRight.getX()) / 2);
        locRegionCenter.setZ((locLowerLeft.getZ() + locUpperRight.getZ()) / 2);
        return locRegionCenter;
    }
    public static location getGoodLocationOutsideOfRegion(region rgnSearchRegion, float fltXSize, float fltZSize, float fltDistance)
    {
        if (rgnSearchRegion == null)
        {
            LOG("mission", "getting good location outside of region returned NULL");
            return null;
        };
        float fltRegionExtent = getRegionExtents(rgnSearchRegion);
        location locRegionCenter = getRegionCenter(rgnSearchRegion);
        location locDestination = utils.getRandomLocationInRing(locRegionCenter, fltRegionExtent, fltRegionExtent + fltDistance);
        location locLowerLeft = new location(locDestination);
        locLowerLeft.setX(locLowerLeft.getX() - GOOD_LOCATION_SEARCH_SIZE + fltXSize);
        locLowerLeft.setZ(locLowerLeft.getZ() - GOOD_LOCATION_SEARCH_SIZE + fltZSize);
        location locUpperRight = new location(locDestination);
        locUpperRight.setX(locUpperRight.getX() + GOOD_LOCATION_SEARCH_SIZE + fltXSize);
        locUpperRight.setZ(locUpperRight.getZ() + GOOD_LOCATION_SEARCH_SIZE + fltZSize);
        return getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, false, false);
    }
    public static location getGoodLocationOutsideOfRegion(region rgnSearchRegion, float fltXSize, float fltZSize, float fltDistance, boolean boolIgnoreWater, boolean boolIgnoreSlope)
    {
        float fltRegionExtent = getRegionExtents(rgnSearchRegion);
        location locRegionCenter = new location(getRegionCenter(rgnSearchRegion));
        location locDestination = new location(utils.getRandomLocationInRing(locRegionCenter, fltRegionExtent, fltRegionExtent + fltDistance));
        location locLowerLeft = new location(locDestination);
        locLowerLeft.setX(locLowerLeft.getX() - GOOD_LOCATION_SEARCH_SIZE + fltXSize);
        locLowerLeft.setZ(locLowerLeft.getZ() - GOOD_LOCATION_SEARCH_SIZE + fltZSize);
        location locUpperRight = new location(locDestination);
        locUpperRight.setX(locUpperRight.getX() + GOOD_LOCATION_SEARCH_SIZE + fltXSize);
        locUpperRight.setZ(locUpperRight.getZ() + GOOD_LOCATION_SEARCH_SIZE + fltZSize);
        return getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, boolIgnoreWater, boolIgnoreSlope);
    }
    public static region getClosestCityRegion(region rgnStartRegion)
    {
        String strName = rgnStartRegion.getName();
        region[] rgnCities = getRegionsWithMunicipal(rgnStartRegion.getPlanetName(), regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            return null;
        }
        region rgnClosestRegion = rgnCities[0];
        float fltDistance;
        float fltLowestDistance = 500000000;
        location locCurrentCenter = getRegionCenter(rgnStartRegion);
        int intI = 0;
        while (intI < rgnCities.length)
        {
            String strNewName = rgnCities[intI].getName();
            if (!strNewName.equals(strName))
            {
                fltDistance = getDistance(locCurrentCenter, getRegionCenter(rgnCities[intI]));
                if (fltDistance < fltLowestDistance)
                {
                    fltLowestDistance = fltDistance;
                    rgnClosestRegion = rgnCities[intI];
                }
            }
            intI++;
        }
        return rgnClosestRegion;
    }
    public static region getDeliverCityRegion(region rgnStartRegion)
    {
        List<String> strPlanets = new ArrayList<>();
        String strPlanet = rgnStartRegion.getPlanetName();
        region[] rgnCities = getRegionsWithMunicipal(strPlanet, regions.MUNI_TRUE);
        List<region> rgnNewCities = new ArrayList<>();
        if (rgnCities == null)
        {
            return null;
        }
        if (rgnCities.length == 0)
        {
            return null;
        }
        if (rgnCities.length < 3)
        {
            for (int intI = 0; intI < PLANETS.length; intI++)
            {
                String strTestString = PLANETS[intI];
                if (!strTestString.equals(strPlanet))
                {
                    strPlanets.add(strTestString);
                }
            }
            String strNewPlanet = strPlanets.get(rand(0, strPlanets.size() - 1));
            rgnCities = getRegionsWithMunicipal(strNewPlanet, regions.MUNI_TRUE);
            if (rgnCities != null && rgnCities.length > 0)
            {
                return rgnCities[rand(0, rgnCities.length - 1)];
            }
            else 
            {
                return null;
            }
        }
        else 
        {
            int intI = 0;
            String strStartName = rgnStartRegion.getName();
            while (intI < rgnCities.length)
            {
                String strNewName = rgnCities[intI].getName();
                if (!strStartName.equals(strNewName))
                {
                    rgnNewCities.add(rgnCities[intI]);
                }
                intI = intI + 1;
            }
            region rgnCity = rgnNewCities.get(rand(0, rgnNewCities.size() - 1));
            return rgnCity;
        }
    }
    public static region getCityRegion(region rgnStartRegion)
    {
        region[] rgnCitiesArray = getRegionsWithMunicipal(rgnStartRegion.getPlanetName(), regions.MUNI_TRUE);
        List<region> rgnCities = new ArrayList<>(Arrays.asList(rgnCitiesArray));
        if (rgnCities == null)
        {
            return null;
        }
        if (rgnCitiesArray == null)
        {
            return null;
        }
        if (rgnCities.size() == 0)
        {
            return null;
        }
        if (rgnCitiesArray.length == 0)
        {
            return null;
        }
        int intI = 0;
        String strStartName = rgnStartRegion.getName();
        while (intI < rgnCities.size())
        {
            String strNewName = rgnCities.get(intI).getName();
            if (strStartName.equals(strNewName))
            {
                rgnCities.remove(intI);
                intI = rgnCities.size() + 10;
            }
            intI++;
        }
        return rgnCities.get(rand(0, rgnCities.size() - 1));
    }
    public static region getCityRegion(location locCurrentLocation)
    {
        region[] rgnCities = getRegionsWithMunicipalAtPoint(locCurrentLocation, regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            return null;
        }
        return rgnCities[rand(0, rgnCities.length - 1)];
    }
    public static boolean isInCity(location locTestLocation)
    {
        return  getRegionsWithMunicipalAtPoint(locTestLocation, regions.MUNI_TRUE) != null;
    }
    public static boolean isInMissionCity(location locTestLocation)
    {
        if (city.isInCity(locTestLocation))
        {
            return true;
        }
        region[] rgnCities = getRegionsWithMunicipalAtPoint(locTestLocation, regions.MUNI_TRUE);
        if (rgnCities != null)
        {
            return true;
        }
        rgnCities = getRegionsWithGeographicalAtPoint(locTestLocation, regions.GEO_CITY);
        if (rgnCities != null)
        {
            return true;
        }
        return false;
    }
    public static location getGoodLocationInRegion(region rgnSearchRegion, float fltXSize, float fltZSize)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        return getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, false, false);
    }
    public static location getGoodLocationInRegion(region rgnSearchRegion, float fltXSize, float fltZSize, boolean boolIgnoreWater, boolean boolIgnoreSlope)
    {
        location[] locExtents = getRegionExtent(rgnSearchRegion);
        location locLowerLeft = new location(locExtents[0]);
        location locUpperRight = new location(locExtents[1]);
        return getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, boolIgnoreWater, boolIgnoreSlope);
    }
    public static location getRandomGoodLocation(location start, float searchMin, float searchMax, float bestSize)
    {
        location result = null;
        location l = utils.getRandomLocationInRing(start, searchMin, searchMax);
        location lowerLeft = new location();
        location upperRight = new location();
        lowerLeft.setX(l.getX() - bestSize);
        lowerLeft.setZ(l.getZ() - bestSize);
        upperRight.setX(l.getX() + bestSize);
        upperRight.setZ(l.getZ() + bestSize);
        float radius = bestSize;
        while (result == null && radius > 2.0f)
        {
            result = getGoodLocation(radius, radius, lowerLeft, upperRight, false, false);
            radius = radius * 0.5f;
        }
        return result;
    }
    public static location getGoodLocationAroundLocation(location locSearchLocation, float fltXSize, float fltZSize, float fltXSearchSize, float fltZSearchSize)
    {
        location locLowerLeft = new location(locSearchLocation);
        locLowerLeft.setX(locLowerLeft.getX() - fltXSearchSize);
        locLowerLeft.setZ(locLowerLeft.getZ() - fltZSearchSize);
        location locUpperRight = new location(locSearchLocation);
        locUpperRight.setX(locUpperRight.getX() + fltXSearchSize);
        locUpperRight.setZ(locUpperRight.getZ() + fltZSearchSize);
        return getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, false, false);
    }
    public static location getGoodLocationAroundLocation(location locSearchLocation, float fltXSize, float fltZSize, float fltXSearchSize, float fltZSearchSize, boolean boolIgnoreWater, boolean boolIgnoreSlope)
    {
        location locLowerLeft = new location(locSearchLocation);
        locLowerLeft.setX(locLowerLeft.getX() - fltXSearchSize);
        locLowerLeft.setZ(locLowerLeft.getZ() - fltZSearchSize);
        location locUpperRight = locSearchLocation;
        locUpperRight.setX(locUpperRight.getX() + fltXSearchSize);
        locUpperRight.setZ(locUpperRight.getZ() + fltZSearchSize);
        location locGoodLocation = getGoodLocation(fltXSize, fltZSize, locLowerLeft, locUpperRight, boolIgnoreWater, boolIgnoreSlope);
        if (locGoodLocation == null)
        {
            LOG("DESIGNER_FATAL", "X search size is " + fltXSearchSize);
            LOG("DESIGNER_FATAL", "Z search size is " + fltZSearchSize);
            LOG("DESIGNER_FATAL", "size of thing is " + fltZSize);
            LOG("DESIGNER_FATAL", "Lower left is " + locLowerLeft);
            LOG("DESIGNER_FATAL", "Upper Right is " + locUpperRight);
            LOG("DESIGNER_FATAL", "Start location is " + locSearchLocation);
        }
        return locGoodLocation;
    }
    public static location getGoodLocationAroundLocationAvoidCollidables(location locSearchLocation, float fltXSize, float fltZSize, float fltXSearchSize, float fltZSearchSize, boolean boolIgnoreWater, boolean boolIgnoreSlope, float staticObjDistance)
    {
        location locLowerLeft = new location(locSearchLocation);
        locLowerLeft.setX(locLowerLeft.getX() - fltXSearchSize);
        locLowerLeft.setZ(locLowerLeft.getZ() - fltZSearchSize);
        location locUpperRight = locSearchLocation;
        locUpperRight.setX(locUpperRight.getX() + fltXSearchSize);
        locUpperRight.setZ(locUpperRight.getZ() + fltZSearchSize);
        location locGoodLocation = getGoodLocationAvoidCollidables(fltXSize, fltZSize, locLowerLeft, locUpperRight, boolIgnoreWater, boolIgnoreSlope, staticObjDistance);
        if (locGoodLocation == null)
        {
            LOG("DESIGNER_FATAL", "X search size is " + fltXSearchSize);
            LOG("DESIGNER_FATAL", "Z search size is " + fltZSearchSize);
            LOG("DESIGNER_FATAL", "size of thing is " + fltZSize);
            LOG("DESIGNER_FATAL", "Lower left is " + locLowerLeft);
            LOG("DESIGNER_FATAL", "Upper Right is " + locUpperRight);
            LOG("DESIGNER_FATAL", "Start location is " + locSearchLocation);
        }
        return locGoodLocation;
    }
    public static String getCityName(location locCurrentLocation)
    {
        region[] rgnCities = getRegionsWithMunicipalAtPoint(locCurrentLocation, regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            return null;
        }
        region rgnCity = rgnCities[0];
        string_id strFictionalName = utils.unpackString(rgnCity.getName());
        String strAsciiId = strFictionalName.getAsciiId();
        return strAsciiId;
    }
    public static String getGuardSpawnerRegionName(location locCurrentLocation)
    {
        if (locCurrentLocation == null)
        {
            return null;
        }
        region[] rgnCities = getRegionsWithGeographicalAtPoint(locCurrentLocation, regions.GEO_CITY);
        if (rgnCities == null)
        {
            return null;
        }
        region rgnCity = rgnCities[0];
        String strName = rgnCity.getName();
        return strName;
    }
    public static location getBountyLocation(String strPlanet)
    {
        region[] rgnCities = getRegionsWithMunicipal(strPlanet, regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            return null;
        }
        if (rgnCities.length > 0)
        {
            int intRoll = rand(0, rgnCities.length - 1);
            region rgnCity = rgnCities[intRoll];
            location locDestination = getGoodCityLocation(rgnCity, strPlanet);
            location locCenter = getRegionCenter(rgnCities[intRoll]);
            return locCenter;
        }
        else 
        {
            location locDestination = new location();
            locDestination.setX(rand(-6500, 6500));
            locDestination.setZ(rand(-6500, 6500));
            locDestination.setArea(strPlanet);
            locDestination = locations.getGoodLocationAroundLocation(locDestination, 1, 1, 400, 400, false, true);
            if (locDestination == null)
            {
                LOG("DESIGNER_FATAL", "No good location found for planet " + strPlanet);
                locDestination = new location();
                locDestination.setArea(strPlanet);
                locDestination.setX(0);
                locDestination.setY(0);
                locDestination.setZ(0);
            }
            return locDestination;
        }
    }
    public static location moveLocationTowardsLocation(location locStartLocation, location locEndLocation, float fltDistance)
    {
        location locNewLocation = new location(locStartLocation);
        float fltTotalDistance = getDistance(locStartLocation, locEndLocation);
        if (fltTotalDistance <= 0)
        {
            return locEndLocation;
        }
        float fltNewX = ((locEndLocation.getX() - locStartLocation.getX()) / fltTotalDistance);
        fltNewX *= fltDistance;
        locNewLocation.setX(locNewLocation.getX() + fltNewX);
        float fltNewZ = ((locEndLocation.getZ() - locStartLocation.getZ()) / fltTotalDistance);
        fltNewZ *= fltDistance;
        locNewLocation.setZ(locNewLocation.getZ() + fltNewZ);
        return locNewLocation;
    }
    public static boolean isCityRegion(region rgnTest)
    {
        if (rgnTest.getMunicipalType() == regions.MUNI_TRUE)
        {
            return true;
        }
        return false;
    }
    public static location getDifferentGoodCityLocation(location locStartLocation)
    {
        region[] rgnCities = getRegionsWithMunicipalAtPoint(locStartLocation, regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            LOG("missions", "no cities found at locStartLocation ");
            return null;
        }
        region rgnCity = rgnCities[0];
        string_id strFictionalName = utils.unpackString(rgnCity.getName());
        String strAsciiId = strFictionalName.getAsciiId();
        int regionType = regions.getDeliverMissionRegionType(strAsciiId);
        rgnCities = getRegionsWithMissionAtPoint(locStartLocation, regionType);
        if (rgnCities == null)
        {
            LOG("missions", "at start location of " + locStartLocation + ", we found no mission types of bestine. ");
            return null;
        }
        String strOldName = rgnCities[0].getName();
        region[] rgnGoodLocationsArray = getRegionsWithMission(locStartLocation.getArea(), regionType);
        List<region> rgnGoodLocations = new ArrayList<>(Arrays.asList(rgnGoodLocationsArray));
        if (rgnGoodLocations == null)
        {
            LOG("missions", "no regions found with mission type on planet " + locStartLocation.getArea());
            LOG("mission_spam", "No regions were foind of type " + regionType);
            return null;
        }
        int intI = 0;
        while (intI < rgnGoodLocations.size())
        {
            String strNewName = rgnGoodLocations.get(intI).getName();
            if (strNewName.equals(strOldName))
            {
                rgnGoodLocations.remove(intI);
                intI = rgnGoodLocations.size() + 19;
            }
            intI = intI + 1;
        }
        region rgnSpawnRegion = rgnGoodLocations.get(rand(0, rgnGoodLocations.size() - 1));
        return findPointInRegion(rgnSpawnRegion);
    }
    public static location getDifferentGoodCityRegionLocation(location locStartLocation)
    {
        region[] rgnCities = getRegionsWithMunicipalAtPoint(locStartLocation, regions.MUNI_TRUE);
        if (rgnCities == null)
        {
            return null;
        }
        region rgnCity = rgnCities[0];
        location locGoodLocation = new location();
        string_id strFictionalName = utils.unpackString(rgnCity.getName());
        String strAsciiId = strFictionalName.getAsciiId();
        int regionType = regions.getDeliverMissionRegionType(strAsciiId);
        rgnCities = getRegionsWithMissionAtPoint(locStartLocation, regionType);
        if (rgnCities == null)
        {
            LOG("missions", "at start location of " + locStartLocation + ", we found no mission types of bestine. ");
            return null;
        }
        String strOldName = rgnCities[0].getName();
        region[] rgnGoodLocationsArray = getRegionsWithMission(locStartLocation.getArea(), regionType);
        List<region> rgnGoodLocations = new ArrayList<>(Arrays.asList(rgnGoodLocationsArray));
        if (rgnGoodLocations == null)
        {
            LOG("missions", "no regions found with mission type on planet " + locStartLocation.getArea());
            LOG("mission_spam", "No regions were foind of type " + regionType);
            return null;
        }
        int intI = 0;
        while (intI < rgnGoodLocations.size())
        {
            String strNewName = rgnGoodLocations.get(intI).getName();
            if (strNewName.equals(strOldName))
            {
                rgnGoodLocations.remove(intI);
                intI = rgnGoodLocations.size() + 19;
            }
            intI = intI + 1;
        }
        region rgnSpawnRegion = rgnGoodLocations.get(rand(0, rgnGoodLocations.size() - 1));
        return getRegionCenter(rgnSpawnRegion);
    }
    public static location getGoodCityLocation(region rgnCity, String strPlanet)
    {
        if (rgnCity == null || strPlanet == null)
        {
            LOG("mission_spam", "getGoodCityLocation(line 883) returned NULL");
            return null;
        }
        location locGoodLocation = new location();
        string_id strFictionalName = utils.unpackString(rgnCity.getName());
        if(strFictionalName == null)
        {
            LOG("mission_spam", "getGoodCityLocation was unable to get the Fictional name from rgnCity (" + rgnCity.getName() + ")");
            return null;
        }
        String strAsciiId = strFictionalName.getAsciiId();
        int regionType = regions.getDeliverMissionRegionType(strAsciiId);
        region[] rgnGoodLocations = getRegionsWithMission(strPlanet, regionType);
        if (rgnGoodLocations == null)
        {
            LOG("mission_spam", "No regions were foind of type " + regionType);
            return null;
        }
        region rgnSpawnRegion = rgnGoodLocations[rand(0, rgnGoodLocations.length - 1)];
        int intI = 0;
        while (intI < rgnGoodLocations.length)
        {
            LOG("locations", "rgnGoodLocationsRegion[intI] Type is " + rgnGoodLocations[intI].getMissionType());
            LOG("locations", "rgnGoodLocationsRegion[intI] planet is " + rgnGoodLocations[intI].getPlanetName());
            intI = intI + 1;
        }
        locGoodLocation = findPointInRegion(rgnSpawnRegion);
        return locGoodLocation;
    }
    public static location getGoodCityRegionLocation(region rgnCity, String strPlanet)
    {
        if (rgnCity == null || strPlanet == null)
        {
            return null;
        }
        string_id strFictionalName = utils.unpackString(rgnCity.getName());
        if(strFictionalName == null){
            LOG("mission_spam", "Can't unpack city name (" + rgnCity.getName() + ") to get fictional name.");
            return null;
        }
        String strAsciiId = strFictionalName.getAsciiId();
        int regionType = regions.getDeliverMissionRegionType(strAsciiId);
        region[] rgnGoodLocations = getRegionsWithMission(strPlanet, regionType);
        if (rgnGoodLocations == null)
        {
            LOG("mission_spam", "No regions were found of type " + regionType);
            return null;
        }
        region rgnSpawnRegion = rgnGoodLocations[rand(0, rgnGoodLocations.length - 1)];
        location locGoodLocation = new location();
        locGoodLocation = getRegionCenter(rgnSpawnRegion);
        return locGoodLocation;
    }
    public static int normalizeDifficultyForRegion(int intDifficulty, location locTest)
    {
        region[] rgnRegionsAtPoint = getRegionsWithSpawnableAtPoint(locTest, regions.SPAWN_TRUE);
        if (rgnRegionsAtPoint == null || rgnRegionsAtPoint.length == 0)
        {
            rgnRegionsAtPoint = getRegionsWithSpawnableAtPoint(locTest, regions.SPAWN_DEFAULT);
            if (rgnRegionsAtPoint == null)
            {
                LOG("DESIGNER_FATAL", "No regions at location " + locTest);
                return intDifficulty;
            }
        }
        region rgnSmallestRegion = getSmallestRegion(rgnRegionsAtPoint);
        if (rgnSmallestRegion == null)
        {
            LOG("locations", "Smallest region was null");
            return intDifficulty;
        }
        int intMinRegionDifficulty = rgnSmallestRegion.getMinDifficultyType();
        int intMaxRegionDifficulty = rgnSmallestRegion.getMaxDifficultyType();
        return rand(intMinRegionDifficulty, intMaxRegionDifficulty);
    }
    public static region[] getDifficultyRegionsAtLocation(location locTest)
    {
        region[] rgnRegionsAtPoint = getRegionsWithSpawnableAtPoint(locTest, regions.SPAWN_TRUE);
        if (rgnRegionsAtPoint == null || rgnRegionsAtPoint.length == 0)
        {
            rgnRegionsAtPoint = getRegionsWithSpawnableAtPoint(locTest, regions.SPAWN_DEFAULT);
            if (rgnRegionsAtPoint == null || rgnRegionsAtPoint.length == 0)
            {
                return null;
            }
        }
        return rgnRegionsAtPoint;
    }
    public static int getMinMissionDifficultyAtLocation(location locTest)
    {
        region[] rgnRegionsAtPoint = getRegionsWithMissionAtPoint(locTest, regions.MISSION_OTHER);
        if (rgnRegionsAtPoint == null || rgnRegionsAtPoint.length == 0)
        {
            rgnRegionsAtPoint = getRegionsWithSpawnableAtPoint(locTest, regions.SPAWN_DEFAULT);
            if (rgnRegionsAtPoint == null || rgnRegionsAtPoint.length == 0)
            {
                return 10;
            }
        }
        int minDifficulty = 90;
        for (int i = 0; i < rgnRegionsAtPoint.length; i++)
        {
            int regionMinDifficulty = rgnRegionsAtPoint[i].getMinDifficultyType();
            if (minDifficulty > regionMinDifficulty)
            {
                minDifficulty = regionMinDifficulty;
            }
        }
        return minDifficulty;
    }
    public static int getMinDifficultyForPlanet(String planet)
    {
        int difficulty = 1;
        if (planet.equals("") || planet.length() <= 0)
        {
            return difficulty;
        }
        dictionary planetLevels = utils.dataTableGetRow(PLANET_LEVEL_TABLE, planet);
        if (planetLevels != null)
        {
            difficulty = planetLevels.getInt("minLevel");
        }
        else 
        {
            difficulty = -1;
        }
        if (difficulty < 1)
        {
            difficulty = 1;
        }
        return difficulty;
    }
    public static int getMaxDifficultyForPlanet(String planet)
    {
        int difficulty = 90;
        if (planet.equals("") || planet.length() <= 0)
        {
            return difficulty;
        }
        dictionary planetLevels = utils.dataTableGetRow(PLANET_LEVEL_TABLE, planet);
        if (planetLevels != null)
        {
            difficulty = planetLevels.getInt("maxLevel");
        }
        else 
        {
            difficulty = -1;
        }
        if (difficulty > 90)
        {
            difficulty = 90;
        }
        return difficulty;
    }
    public static int getMinDifficultyForLocation(location locTest)
    {
        region[] rgnRegionList = getDifficultyRegionsAtLocation(locTest);
        if (rgnRegionList == null || rgnRegionList.length == 0)
        {
            return 0;
        }
        int minDifficulty = Integer.MAX_VALUE;
        for (int i = 0; i < rgnRegionList.length; i++)
        {
            int regionMinDifficulty = rgnRegionList[i].getMinDifficultyType();
            if (minDifficulty > regionMinDifficulty)
            {
                minDifficulty = regionMinDifficulty;
            }
        }
        return minDifficulty;
    }
    public static int getMaxDifficultyForLocation(location locTest)
    {
        region[] rgnRegionList = getDifficultyRegionsAtLocation(locTest);
        if (rgnRegionList == null || rgnRegionList.length == 0)
        {
            return Integer.MAX_VALUE;
        }
        int maxDifficulty = Integer.MIN_VALUE;
        for (int i = 0; i < rgnRegionList.length; i++)
        {
            int regionMaxDifficulty = rgnRegionList[i].getMaxDifficultyType();
            if (maxDifficulty < regionMaxDifficulty)
            {
                maxDifficulty = regionMaxDifficulty;
            }
        }
        return maxDifficulty;
    }
    public static int capMinDifficultyForLocation(location locTest, int intDifficulty)
    {
        int intMinRegionDifficulty = getMinDifficultyForLocation(locTest);
        if (intDifficulty < intMinRegionDifficulty)
        {
            intDifficulty = intMinRegionDifficulty;
        }
        return intDifficulty;
    }
    public static int capMaxDifficultyForLocation(location locTest, int intDifficulty)
    {
        int intMaxRegionDifficulty = getMaxDifficultyForLocation(locTest);
        if (intDifficulty > intMaxRegionDifficulty)
        {
            intDifficulty = intMaxRegionDifficulty;
        }
        return intDifficulty;
    }
    public static boolean destroyLocationObject(obj_id locationObject)
    {
        if (isIdValid(locationObject))
        {
            messageTo(locationObject, "handlerDestroyLocationObject", null, 1, false);
            return true;
        }
        return false;
    }
    public static String getBuildoutAreaName(obj_id object)
    {
        if (!isIdValid(object) || !exists(object))
        {
            return NO_AREA;
        }
        return getBuildoutAreaName(getLocation(trial.getTop(object)));
    }
    public static String getBuildoutAreaName(location loc)
    {
        if (isIdValid(loc.getCell()))
        {
            obj_id topBuilding = trial.getTop(loc.getCell());
            loc = getLocation(topBuilding);
        }
        String buildout_table = "datatables/buildout/areas_" + loc.getArea() + ".iff";
        float locX = loc.getX();
        float locZ = loc.getZ();
        if (loc.getArea().indexOf("space_npe_falcon") >= 0 || !dataTableOpen(buildout_table))
        {
            return NO_AREA;
        }
        int rows = dataTableGetNumRows(buildout_table);
        String area_name = NO_AREA;
        for (int i = 0; i < rows; i++)
        {
            dictionary dict = dataTableGetRow(buildout_table, i);
            float xMin = dict.getFloat("x1");
            float xMax = dict.getFloat("x2");
            float zMin = dict.getFloat("z1");
            float zMax = dict.getFloat("z2");
            if (locX >= xMin && locX <= xMax && locZ >= zMin && locZ <= zMax)
            {
                area_name = dict.getString("area");
            }
        }
        return area_name;
    }
    public static int getBuildoutAreaRow(obj_id object)
    {
        if (!isIdValid(object) || !exists(object))
        {
            return -1;
        }
        return getBuildoutAreaRow(getLocation(trial.getTop(object)));
    }
    public static int getBuildoutAreaRow(location loc)
    {
        if (isIdValid(loc.getCell()))
        {
            obj_id topBuilding = trial.getTop(loc.getCell());
            loc = getLocation(topBuilding);
        }
        String buildout_table = "datatables/buildout/areas_" + loc.getArea() + ".iff";
        float locX = loc.getX();
        float locZ = loc.getZ();
        if (loc.getArea().indexOf("space_npe_falcon") >= 0 || !dataTableOpen(buildout_table))
        {
            return -1;
        }
        int rows = dataTableGetNumRows(buildout_table);
        int row = -1;
        for (int i = 0; i < rows; i++)
        {
            dictionary dict = dataTableGetRow(buildout_table, i);
            float xMin = dict.getFloat("x1");
            float xMax = dict.getFloat("x2");
            float zMin = dict.getFloat("z1");
            float zMax = dict.getFloat("z2");
            if (locX >= xMin && locX <= xMax && locZ >= zMin && locZ <= zMax)
            {
                row = i;
            }
        }
        return row;
    }
    public static boolean isInRegion(obj_id player, String regionName)
    {
        region[] regionList = getRegionsAtPoint(getLocation(player));
        obj_id planetId = getPlanetByName(getLocation(player).getArea());
        if (regionList == null || regionList.length == 0)
        {
            return false;
        }
        for (int i = 0; i < regionList.length; i++)
        {
            if (regionList[i] == null || (regionList[i].getName()).length() <= 0)
            {
                continue;
            }
            if (regionName.equals(regionList[i].getName()))
            {
                return true;
            }
        }
        return false;
    }
}
