package script.library;

import script.*;

public class spam extends script.base_script
{
    public spam()
    {
    }
    public static final String STF_SPAM = "spam";
    public static final string_id PROSE_LOOT_ITEM_SELF = new string_id(STF_SPAM, "loot_item_self");
    public static final string_id PROSE_LOOT_ITEM_OTHER = new string_id(STF_SPAM, "loot_item_other");
    public static void lootItem(obj_id player, obj_id item, obj_id corpseId, boolean toGroup)
    {
        if (!isIdValid(player) || !isIdValid(item) || !isIdValid(corpseId))
        {
            return;
        }
        if (toGroup)
        {
            obj_id gid = getGroupObject(player);
            if (isIdValid(gid))
            {
                prose_package ppOther = new prose_package(PROSE_LOOT_ITEM_OTHER);
                ppOther.setTU(player);
                ppOther.setTT(item);
                ppOther.setTO(corpseId);
                group.sendGroupChatMessage(gid, ppOther);
            }
        }
    }
    public static void lootItem(obj_id player, obj_id item, obj_id corpseId)
    {
        lootItem(player, item, corpseId, true);
    }
}
