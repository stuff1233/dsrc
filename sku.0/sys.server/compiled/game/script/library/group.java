package script.library;

import script.*;
import java.util.ArrayList;
import java.util.List;

public class group extends script.base_script
{
    public group()
    {
    }
    public static final String SCRIPT_GROUP_OBJECT = "grouping.group_object";
    public static final String SCRIPT_GROUP_MEMBER = "grouping.group_member";
    public static final String GROUP_STF = "group";
    public static final string_id PROSE_NOTIFY_UNKNOWN = new string_id("group", "notify_unknown");
    public static final string_id PROSE_NOTIFY_COIN_LOOT_INT = new string_id("group", "notify_coin_loot_int");
    public static final string_id PROSE_NOTIFY_COIN_LOOT_STRING = new string_id("group", "notify_coin_loot_string");
    public static final string_id PROSE_NOTIFY_PARTIAL_COIN_LOOT = new string_id("group", "notify_partial_coin_loot_int");
    public static final string_id PROSE_NOTIFY_ITEM_LOOT = new string_id("group", "notify_item_loot");
    public static final string_id PROSE_NOTIFY_PARTIAL_LOOT = new string_id("group", "notify_partial_loot");
    public static final string_id PROSE_NOTIFY_NO_LOOT = new string_id("group", "notify_no_loot");
    public static final string_id PROSE_NOTIFY_ERROR_LOOT = new string_id("group", "notify_error_loot");
    public static final string_id PROSE_NOTIFY_SINGLE_LOOT = new string_id("group", "notify_single_loot");
    public static final string_id PROSE_NOTIFY_HARVEST_CORPSE = new string_id("group", "notify_harvest_corpse");
    public static final string_id PROSE_NOTIFY_INCAP = new string_id("group", "notify_incap");
    public static final string_id PROSE_NOTIFY_DEATH = new string_id("group", "notify_death");
    public static final string_id PROSE_NOTIFY_CLONED = new string_id("group", "notify_cloned");
    public static final string_id PROSE_NOTIFY_CLONED_CITY = new string_id("group", "notify_cloned_city");
    public static final string_id PROSE_NOTIFY_OPTION_ON = new string_id("group", "notify_option_on");
    public static final string_id PROSE_NOTIFY_OPTION_OFF = new string_id("group", "notify_option_off");
    public static final string_id PROSE_SPLIT_COINS_SELF = new string_id("group", "prose_split_coins_self");
    public static final string_id PROSE_SPLIT_COINS_MISSION = new string_id("group", "prose_split_coins_mission");
    public static final float SPLIT_RANGE = 200f;
    public static final String HANDLER_SPLIT_SUCCESS = "handleSplitSuccess";
    public static final String HANDLER_SPLIT_FAILURE = "handleSplitFailure";
    public static final byte FREE_FOR_ALL = 0;
    public static final byte MASTER_LOOTER = 1;
    public static final byte LOTTERY = 2;
    public static final byte RANDOM = 3;
    public static String getGroupChatRoomName(obj_id gid)
    {
        return isIdValid(gid) ? ("SWG." + getGalaxyName() + ".group." + gid + ".GroupChat") : null;
    }
    public static void sendGroupChatMessage(obj_id gid, prose_package msg)
    {
        String oobPP = packOutOfBandProsePackage(msg);
        String gRoom = getGroupChatRoomName(gid);
        chatSendToRoom(gRoom, null, oobPP);
    }
    public static boolean isGroupObject(obj_id gid)
    {
        return isIdValid(gid) ? (getGameObjectType(gid) == GOT_group) : false;
    }
    public static boolean isGrouped(obj_id target)
    {
        return isIdValid(target) ? isIdValid(getGroupObject(target)) : false;
    }
    public static boolean inSameGroup(obj_id target1, obj_id target2)
    {
        if (!isIdValid(target1) || !isIdValid(target2))
        {
            return false;
        }
        if (target1 == target2)
        {
            return true;
        }
        if (beast_lib.isBeastMaster(target1))
        {
            obj_id beast = beast_lib.getBeastOnPlayer(target1);
            if (beast == target2)
            {
                return true;
            }
        }
        if (beast_lib.isBeastMaster(target2))
        {
            obj_id beast = beast_lib.getBeastOnPlayer(target2);
            if (beast == target1)
            {
                return true;
            }
        }
        obj_id gid1 = getGroupObject(target1);
        obj_id gid2 = getGroupObject(target2);
        if (!isIdValid(gid1))
        {
            if (beast_lib.isBeast(target1))
            {
                target1 = getMaster(target1);
                if (target1 == target2)
                {
                    return true;
                }
                gid1 = getGroupObject(target1);
                if (!isIdValid(gid1))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        if (!isIdValid(gid2))
        {
            if (beast_lib.isBeast(target2))
            {
                target2 = getMaster(target2);
                if (target1 == target2)
                {
                    return true;
                }
                gid2 = getGroupObject(target2);
                if (!isIdValid(gid2))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        return (gid1 == gid2);
    }
    public static obj_id getLeader(obj_id target)
    {
        if (isIdValid(target))
        {
            obj_id gid;
            if (isGroupObject(target))
            {
                gid = target;
            }
            else
            {
                gid = getGroupObject(target);
            }
            if (isIdValid(gid))
            {
                return getGroupLeaderId(gid);
            }
        }
        return null;
    }
    public static boolean isLeader(obj_id target)
    {
        if (isIdValid(target))
        {
            obj_id gid = getGroupObject(target);
            if (isIdValid(gid))
            {
                return getGroupLeaderId(gid) == target;
            }
        }
        return false;
    }
    public static void changeLeader(obj_id self)
    {
        obj_id groupid = getGroupObject(self);
        if (isIdValid(groupid) && isLeader(self))
        {
            obj_id[] members = getGroupMemberIds(groupid);
            if (members != null && members.length > 0)
            {
                boolean found = false;
                for (int i = 0, j = members.length; i < j && !found; i++)
                {
                    if (!isIdValid(members[i]))
                    {
                        continue;
                    }
                    if (members[i] != self)
                    {
                        found = true;
                        queueCommand(self, (-1818569340), members[i], "", COMMAND_PRIORITY_FRONT);
                    }
                }
            }
        }
    }
    public static void leaveGroup(obj_id player)
    {
        changeLeader(player);
        queueCommand(player, (1348589140), player, "", COMMAND_PRIORITY_FRONT);
    }
    public static boolean isMixedFactionGroup(obj_id groupId)
    {
        if (!isIdValid(groupId))
        {
            return false;
        }
        obj_id[] members = getGroupMemberIds(groupId);
        if (members != null && members.length > 0)
        {
            int faction = factions.FACTION_FLAG_UNKNOWN;
            for (int i = 0, j = members.length; i < j; i++)
            {
                if (!isIdValid(members[i]) || !exists(members[i]))
                {
                    continue;
                }
                if (faction == factions.FACTION_FLAG_UNKNOWN)
                {
                    faction = factions.getFactionFlag(members[i]);
                }
                if (faction != factions.getFactionFlag(members[i]))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public static String getCreatureGroupName(obj_id target)
    {
        if (isIdValid(target))
        {
            obj_id gid = getGroupObject(target);
            if (isIdValid(gid))
            {
                return getGroupName(gid);
            }
        }
        return null;
    }
    public static ArrayList getPCMembersInRange(obj_id actor, float range)
    {
        if (!isIdValid(actor) || !exists(actor) || !actor.isLoaded())
        {
            return null;
        }
        obj_id gid = getGroupObject(actor);
        if (!isIdValid(gid))
        {
            return null;
        }
        ArrayList<obj_id> targets = new ArrayList<>();
        obj_id[] members = getGroupMemberIds(gid);
        if (members != null && members.length > 0)
        {
            location here = getWorldLocation(actor);
            for (int i = 0; i < members.length; i++)
            {
                obj_id member = members[i];
                if (isIdValid(member) && member.isLoaded() && isPlayer(member))
                {
                    location there = getWorldLocation(member);
                    if (getDistance(here, there) <= range)
                    {
                        targets.add(member);
                    }
                }
            }
        }
        return targets;
    }
    public static ArrayList getPCMembersInRange()
    {
        return getPCMembersInRange(getSelf());
    }
    public static ArrayList getPCMembersInRange(float range)
    {
        return getPCMembersInRange(getSelf(), range);
    }
    public static ArrayList getPCMembersInRange(obj_id actor)
    {
        return getPCMembersInRange(actor, 200f);
    }
    public static void notifyCoinLoot(obj_id gid, obj_id actor, obj_id target, int amt)
    {
        if (!isIdValid(gid) || !isIdValid(actor) || !isIdValid(target))
        {
            return;
        }
        if (amt < 1)
        {
            return;
        }
        prose_package ppCoinLoot = new prose_package(PROSE_NOTIFY_COIN_LOOT_INT);
        ppCoinLoot.setTU(actor);
        ppCoinLoot.setTT(target);
        ppCoinLoot.setDI(amt);
        sendGroupChatMessage(gid, ppCoinLoot);
    }
    public static void notifyCoinLootFail(obj_id gid, obj_id actor, obj_id target, int amt)
    {
        if (!isIdValid(gid) || !isIdValid(actor) || !isIdValid(target))
        {
            return;
        }
        if (amt < 1)
        {
            return;
        }
        int remainder = getCashBalance(target);
        prose_package ppPartialCoinLoot = new prose_package(PROSE_NOTIFY_PARTIAL_COIN_LOOT);
        ppPartialCoinLoot.setTU(actor);
        ppPartialCoinLoot.setTT(target);
        ppPartialCoinLoot.setDI(amt);
        sendGroupChatMessage(gid, ppPartialCoinLoot);
        sendSystemMessageProse(actor, ppPartialCoinLoot);
    }
    public static void notifyItemLoot(obj_id gid, obj_id actor, obj_id target, obj_id item)
    {
        if (!isIdValid(gid))
        {
            return;
        }
        if (!isIdValid(actor))
        {
            return;
        }
        if (!isIdValid(target))
        {
            return;
        }
        if (!isIdValid(item))
        {
            return;
        }
        prose_package ppSingleLoot = new prose_package(PROSE_NOTIFY_SINGLE_LOOT);
        ppSingleLoot.setTU(actor);
        ppSingleLoot.setTT(target);
        ppSingleLoot.setTO(item);

        obj_id[] objMembersWhoExist = utils.getLocalGroupMemberIds(gid);
        for (int x = 0; x < objMembersWhoExist.length; x++)
        {
            sendSystemMessageProse(objMembersWhoExist[x], ppSingleLoot);
        }
    }
    public static void notifyItemLoot(obj_id gid, obj_id actor, obj_id target, obj_id[] items, int leftCount)
    {
        if (!isIdValid(gid) || !isIdValid(actor) || !isIdValid(target))
        {
            return;
        }
        if (items == null || items.length == 0)
        {
            prose_package ppNoLoot = new prose_package(PROSE_NOTIFY_NO_LOOT);
            ppNoLoot.setTU(actor);
            ppNoLoot.setTT(target);
            ppNoLoot.setDI(leftCount);
            sendSystemMessageProse(gid, ppNoLoot);
        }
        else
        {
            int cnt = items.length;
            prose_package ppItemLoot = new prose_package(PROSE_NOTIFY_ITEM_LOOT);
            ppItemLoot.setTU(actor);
            ppItemLoot.setTT(target);
            ppItemLoot.setDI(cnt);
            sendSystemMessageProse(gid, ppItemLoot);
            for (int i = 0; i < items.length; i++)
            {
                notifyItemLoot(gid, actor, target, items[i]);
            }
        }
        if (leftCount > 0)
        {
            prose_package ppNoLoot = new prose_package(PROSE_NOTIFY_PARTIAL_LOOT);
            ppNoLoot.setTU(actor);
            ppNoLoot.setTT(target);
            ppNoLoot.setDI(leftCount);
            sendSystemMessageProse(gid, ppNoLoot);
        }
    }
    public static void notifyItemLoot(obj_id gid, obj_id actor, obj_id target, obj_id[] items)
    {
        notifyItemLoot(gid, actor, target, items, 0);
    }
    public static void notifyHarvest(obj_id gid, obj_id actor, obj_id target, String rType, int amt)
    {
        if (!isIdValid(gid) || !isIdValid(actor) || !isIdValid(target))
        {
            return;
        }
        if (rType == null || rType.isEmpty())
        {
            return;
        }
        if (amt < 1)
        {
            return;
        }
        prose_package ppHarvest = new prose_package(PROSE_NOTIFY_HARVEST_CORPSE);
        ppHarvest.setTU(actor);
        ppHarvest.setTT(target);
        ppHarvest.setTO(rType);
        ppHarvest.setDI(amt);
        sendGroupChatMessage(gid, ppHarvest);
    }
    public static void notifyIncapacitation(obj_id gid, obj_id actor)
    {
        if (!isIdValid(gid) || !isIdValid(actor))
        {
            return;
        }
        prose_package ppIncap = new prose_package(PROSE_NOTIFY_INCAP);
        ppIncap.setTU(actor);
        sendGroupChatMessage(gid, ppIncap);
    }
    public static void notifyDeath(obj_id gid, obj_id actor)
    {
        if (!isIdValid(gid) || !isIdValid(actor))
        {
            return;
        }
        prose_package ppDeath = new prose_package(PROSE_NOTIFY_DEATH);
        ppDeath.setTU(actor);
        sendGroupChatMessage(gid, ppDeath);
    }
    public static void notifyCloned(obj_id gid, obj_id actor)
    {
        if (!isIdValid(gid) || !isIdValid(actor))
        {
            return;
        }
        location there = getWorldLocation(actor);
        if (there != null)
        {
            String cityName = planetary_map.getCityRegionName(there);
            if (cityName != null && !cityName.isEmpty())
            {
                prose_package ppClonedCity = new prose_package(PROSE_NOTIFY_CLONED_CITY);
                ppClonedCity.setTU(actor);
                ppClonedCity.setTO(cityName);
                sendGroupChatMessage(gid, ppClonedCity);
                return;
            }
        }
        prose_package ppCloned = new prose_package(PROSE_NOTIFY_CLONED);
        ppCloned.setTU(actor);
        sendGroupChatMessage(gid, ppCloned);
    }
    public static void splitCoins(int amt, dictionary params)
    {
        obj_id actor = getSelf();
        if (!isIdValid(actor) || amt < 1)
        {
            return;
        }
        obj_id gid = getGroupObject(actor);
        if (!isIdValid(gid))
        {
            return;
        }
        if (params == null)
        {
            params = new dictionary();
        }
        int totalDividends = 0;
        List<obj_id> targets = getPCMembersInRange(SPLIT_RANGE);
        if (targets != null && targets.size() > 0)
        {
            int dividend = (int)(amt / targets.size());
            params.put("target", actor);
            params.put("amt", dividend);
            for (int i = 0; i < targets.size(); i++) {
                if (targets.get(i) != actor) {
                    messageTo(targets.get(i), "handleRequestSplitShare", params, 0, false);
                    totalDividends += dividend;
                }
            }
        }
        prose_package ppSplit = new prose_package(PROSE_SPLIT_COINS_SELF);
        sendSystemMessageProse(actor, ppSplit);
    }
    public static void splitCoins(int amt)
    {
        splitCoins(amt, null);
    }
    public static void splitBank(int amt, dictionary params)
    {
        obj_id actor = getSelf();
        if (!isIdValid(actor) || amt < 1)
        {
            return;
        }
        obj_id gid = getGroupObject(actor);
        if (!isIdValid(gid))
        {
            return;
        }
        if (params == null)
        {
            params = new dictionary();
        }
        List<obj_id> targets = getPCMembersInRange(SPLIT_RANGE);
        if (targets != null && targets.size() > 0)
        {
            int dividend = (int)(amt / targets.size());
            params.put("target", actor);
            params.put("amt", dividend);
            if (targets.size() > 0) {
                for (int i = 0; i < targets.size(); i++) {
                    if (targets.get(i) != actor) {
                        messageTo(targets.get(i), "handleRequestPayoutShare", params, 0, false);
                    } else {
                        messageTo(actor, HANDLER_SPLIT_SUCCESS, params, 0, false);
                    }
                }
            }
        }
    }
    public static void splitBank(int amt)
    {
        splitBank(amt, null);
    }
    public static boolean systemPayoutToGroup(String acct, obj_id player, int amt, String reason, String returnHandler, dictionary params)
    {
        return systemPayoutToGroupInternal(acct, player, amt, reason, null, returnHandler, 0.0f, params);
    }
    public static boolean systemPayoutToGroup(String acct, obj_id player, int amt, string_id reasonId, String returnHandler, dictionary params)
    {
        return systemPayoutToGroupInternal(acct, player, amt, null, reasonId, returnHandler, 0.0f, params);
    }
    public static boolean systemPayoutToGroup(String acct, obj_id player, int amt, string_id reasonId, String returnHandler, float divisor, dictionary params)
    {
        return systemPayoutToGroupInternal(acct, player, amt, null, reasonId, returnHandler, divisor, params);
    }
    public static boolean systemPayoutToGroupInternal(String acct, obj_id player, int amt, String reason, string_id reasonId, String returnHandler, float divisor, dictionary params)
    {
        return systemPayoutToGroupInternal(acct, player, amt, null, reasonId, returnHandler, divisor, params, obj_id.NULL_ID);
    }
    public static boolean systemPayoutToGroupInternal(String acct, obj_id player, int amt, String reason, string_id reasonId, String returnHandler, float divisor, dictionary params, obj_id objMissionData)
    {
        if (acct == null || acct.isEmpty())
        {
            return false;
        }
        if (!isIdValid(player))
        {
            return false;
        }
        if (amt < 1)
        {
            return false;
        }
        if (returnHandler == null || returnHandler.equals(""))
        {
            return false;
        }
        if (params == null)
        {
            params = new dictionary();
        }
        obj_id gid = getGroupObject(player);
        if (!isIdValid(gid))
        {
            return false;
        }
        obj_id self = getSelf();
        params.put("msg.id", self);
        params.put("msg.handler", returnHandler);
        if (reason != null && !reason.equals(""))
        {
            params.put("reason", reason);
        }
        if (reasonId != null)
        {
            params.put("reasonId", reasonId);
        }
        params.put(money.DICT_ACCT_NAME, acct);
        params.put(money.DICT_AMOUNT, amt);
        LOG("NewMission", "Payout of " + amt);
        if (!acct.isEmpty() && amt > 0)
        {
            LOG("NewMission", "Distribute " + amt);
            boolean allReceived = distributeMoneyToGroup(player, amt, SPLIT_RANGE, acct, divisor, params, objMissionData);
            string_id partialPayment = new string_id("mission/mission_generic", "group_too_far");
            if (reasonId == null)
            {
                CustomerServiceLog("Mission", "We transferred " + amt + " credits to a group, but there was no message to tell them that", player);
            }
            else
            {
                obj_id[] gmembers = group.getGroupMemberIds(gid);
                if (gmembers != null && gmembers.length > 0)
                {
                    for (int i = 0; i < gmembers.length; ++i)
                    {
                        sendSystemMessage(gmembers[i], reasonId);
                        if (!allReceived)
                        {
                            sendSystemMessage(gmembers[i], partialPayment);
                        }
                    }
                }
            }
        }
        return true;
    }
    public static boolean distributeMoneyToGroup(obj_id player, int amt, float range, String acct, float divisorInput, dictionary params)
    {
        return distributeMoneyToGroup(player, amt, range, acct, divisorInput, params, obj_id.NULL_ID);
    }
    public static boolean distributeMoneyToGroup(obj_id player, int amt, float range, String acct, float divisorInput, dictionary params, obj_id objMissionData)
    {
        if (amt < 1)
        {
            return false;
        }
        obj_id groupObject = getGroupObject(player);
        if (!isIdValid(groupObject))
        {
            return false;
        }
        if (params == null)
        {
            params = new dictionary();
        }
        int missionLevel = params.getInt("intPlayerDifficulty");
        List<obj_id> targets = getPCMembersInRange(player, range);
        if (targets != null && targets.size() > 0)
        {
            float divisor = divisorInput;
            if (divisor < 1.0f)
            {
                divisor = targets.size();
            }
            string_id ppString = PROSE_SPLIT_COINS_SELF;
            for (int i = 0; i < targets.size(); i++)
            {
                if (missions.isDestroyMission(objMissionData))
                {
                    if (targets.size() > 1)
                    {
                        divisor = missions.alterMissionPayoutDivisor(targets.get(i), divisor, missionLevel);
                    }
                    missions.incrementDaily(targets.get(i));
                    ppString = PROSE_SPLIT_COINS_MISSION;
                }
                int payout = amt / ((int)divisor);
                transferBankCreditsFromNamedAccount(acct, targets.get(i), payout, "succ", "noHandler", params);
                prose_package ppSplit = new prose_package(ppString);
                sendSystemMessageProse(targets.get(i), ppSplit);
            }
        }
        return (targets.size() >= getGroupSize(groupObject));
    }
    public static void destroyGroupWaypoint(obj_id player)
    {
        obj_id groupWaypoint = getObjIdObjVar(player, "groupWaypoint");
        if (isIdValid(groupWaypoint))
        {
            destroyWaypointInDatapad(groupWaypoint, player);
            removeObjVar(player, "groupWaypoint");
        }
    }
    public static boolean distributeMissionXpToGroup(obj_id player, float range, obj_id objMissionData)
    {
        obj_id groupObject = getGroupObject(player);
        if (!isIdValid(groupObject) || !isIdValid(objMissionData))
        {
            return false;
        }
        int missionLevel = getIntObjVar(objMissionData, "intPlayerDifficulty");
        List<obj_id> targets = getPCMembersInRange(player, range);
        if (targets != null && targets.size() > 0)
        {
            for (int i = 0; i < targets.size(); i++)
            {
                if (missions.isDestroyMission(objMissionData) && missions.canEarnDailyMissionXp(targets.get(i)))
                {
                    xp.grantMissionXp(targets.get(i), missionLevel);
                }
            }
        }
        return targets.size() >= getGroupSize(groupObject);
    }
}
