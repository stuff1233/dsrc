package script.library;

import script.location;
import script.obj_id;
import script.string_id;

public class trainerlocs extends script.base_script
{
    public trainerlocs()
    {
    }
    public static obj_id getTrehlaKeeloLocation(obj_id player, String profession, String track)
    {
        location targetLoc = new location();
        targetLoc.setArea(track);
        String waypName = "Trehla Keelo";
        targetLoc.setX(3484f);
        targetLoc.setY(0f);
        targetLoc.setZ(4808f);
        obj_id wayp = createWaypointInDatapad(player, targetLoc);
        String playerArea = getLocation(player).getArea();
        if (playerArea != null && playerArea.equals(targetLoc.getArea()))
        {
            setWaypointActive(wayp, true);
        }
        setWaypointName(wayp, waypName);
        return wayp;
    }
    public static obj_id getTrainerLocationWaypoint(obj_id player, String profession, String track)
    {
        location targetLoc = new location();
        targetLoc.setArea(track);
        String waypName;
        switch (track) {
            case "naboo":
                switch (profession) {
                    case "imperial":
                        targetLoc.setX(5182f);
                        targetLoc.setY(-192f);
                        targetLoc.setZ(6750f);
                        waypName = "barn_sinkko";
                        break;
                    case "rebel":
                        targetLoc.setX(4767f);
                        targetLoc.setY(4.22f);
                        targetLoc.setZ(-4812f);
                        waypName = "v3_fx";
                        break;
                    default:
                        targetLoc.setX(-5495f);
                        targetLoc.setY(14.00f);
                        targetLoc.setZ(4476f);
                        waypName = "dinge";
                        break;
                }
                break;
            case "corellia":
                switch (profession) {
                    case "imperial":
                        targetLoc.setX(-2184f);
                        targetLoc.setY(20.0f);
                        targetLoc.setZ(2273f);
                        targetLoc.setArea("talus");
                        waypName = "hakassha_sireen";
                        break;
                    case "rebel":
                        targetLoc.setX(-5170f);
                        targetLoc.setY(21.00f);
                        targetLoc.setZ(-2295f);
                        waypName = "kreezo";
                        break;
                    default:
                        targetLoc.setX(-275f);
                        targetLoc.setY(28f);
                        targetLoc.setZ(-4695f);
                        waypName = "rhea";
                        break;
                }
                break;
            default:
                switch (profession) {
                    case "imperial":
                        targetLoc.setX(-1132f);
                        targetLoc.setY(13.32f);
                        targetLoc.setZ(-3542f);
                        waypName = "akal_colzet";
                        break;
                    case "rebel":
                        targetLoc.setX(-2991f);
                        targetLoc.setY(5f);
                        targetLoc.setZ(2123f);
                        waypName = "da_la_socuna";
                        break;
                    default:
                        targetLoc.setX(3381f);
                        targetLoc.setY(5f);
                        targetLoc.setZ(-4799f);
                        waypName = "dravis";
                        break;
                }
                break;
        }
        obj_id wayp = createWaypointInDatapad(player, targetLoc);
        String playerArea = getLocation(player).getArea();
        if (playerArea != null && playerArea.equals(targetLoc.getArea()))
        {
            setWaypointActive(wayp, true);
        }
        setWaypointName(wayp, utils.packStringId(new string_id("npc_spawner_n", waypName)));
        return wayp;
    }
}
