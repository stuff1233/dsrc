/*
 Data element for menu array
*/

package script;

import java.io.Serializable;

public class menu_info_data implements Serializable
{
	private final static long serialVersionUID = -3689934288561342691L;

	private int       id       =  0;
	private int       parent   =  0;
	private int       type     =  0;
	private string_id label    =  null;
	private boolean   enabled      = true;
	private boolean   serverNotify = false;

	public menu_info_data()
	{
		label = new string_id("", "");
	}

	public menu_info_data (int id, int parent, int type, string_id stringId, boolean enabled, boolean serverNotify)
	{
		this.id       = id;
		this.parent   = parent;
		this.type     = type;
		this.enabled  = enabled;
		this.serverNotify = serverNotify;
		label = stringId;
	}

	//-----------------------------------------------------------------------------

	public String toString ()
	{
		return "{menu_info_data id=" + id + ", parent=" + parent + ", type=" + type + ", label=" + label + ", enabled=" + enabled + ", servernotify=" + serverNotify + "}";
	}

	//-----------------------------------------------------------------------------

	/**
	* get the item's id number
	*/

	public int getId ()
	{
		return id;
	}

	//-----------------------------------------------------------------------------

	/**
	* get the item's parent id number.  0 indicates no parent
	*/

	public int getParentId ()
	{
		return parent;
	}

	//-----------------------------------------------------------------------------

	/**
	* get the item's canonical name.  This is used to identify the menu selection for processing.
	*/

	public int getType ()
	{
		return type;
	}

	//-----------------------------------------------------------------------------

	/**
	* get the item's string id for label display.  This is localized at the last moment before the server sends the data to the client,
	* and if it fails, the client attempts to localize the string
	*/

	public string_id getLabel ()
	{
		return label;
	}

	//-----------------------------------------------------------------------------

	/**
	* Sets the label of a menu item
	*
	*/

	public void setLabel (string_id label)
	{
		this.label = label;
	}

	//-----------------------------------------------------------------------------

	/**
	* is the menu item enabled?  Enabled means that the item is in rane.
	*/

	public boolean getEnabled ()
	{
		return enabled;
	}

	//-----------------------------------------------------------------------------

	/**
	* Set the enabled status of the menu item.  Use with care.
	*/

	public void setEnabled (boolean b)
	{
		enabled = b;
	}

	//-----------------------------------------------------------------------------

	/**
	* The menu item cause a server callback to OnObjectMenuSelected ()
	*/

	public boolean getServerNotify ()
	{
		return serverNotify;
	}

	//-----------------------------------------------------------------------------

	/**
	* Set the serverNotify status of the menu item.
	*/

	public void setServerNotify (boolean b)
	{
		serverNotify = b;
	}
}
