package script.ai;

import script.*;

public class beast_inventory extends script.base_script
{
    public int OnReceivedItem(obj_id self, obj_id srcContainer, obj_id transferer, obj_id item)
    {
        if (!isPlayer(transferer))
        {
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_OVERRIDE;
    }
    public int OnAboutToLoseItem(obj_id self, obj_id destContainer, obj_id transferer, obj_id item)
    {
        return SCRIPT_OVERRIDE;
    }
}
