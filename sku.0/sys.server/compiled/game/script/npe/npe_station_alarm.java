package script.npe;

import script.*;
import java.util.ArrayList;
import java.util.List;
import script.library.utils;

public class npe_station_alarm extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        messageTo(self, "doPreloadRequest", null, 1, false);
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        messageTo(self, "doPreloadRequest", null, 1, false);
        return SCRIPT_CONTINUE;
    }
    public int OnPreloadComplete(obj_id self)
    {
        obj_id building = getTopMostContainer(self);
        LOG("npe_alarm", "on attach fired, and building is " + building);
        List<obj_id> objAlarms = new ArrayList<>();
        if (utils.hasScriptVar(building, "objAlarms"))
        {
            objAlarms = utils.getResizeableObjIdArrayScriptVar(building, "objAlarms");
        }
        objAlarms.add(self);
        utils.setScriptVar(building, "objAlarms", objAlarms);
        return SCRIPT_CONTINUE;
    }
    public int doPreloadRequest(obj_id self, dictionary params)
    {
        requestPreloadCompleteTrigger(self);
        return SCRIPT_CONTINUE;
    }
}
