package script.npe;

import script.*;

import script.library.utils;

public class npe_guard1_sequence extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        setInvulnerable(self, true);
        messageTo(self, "npeSetName", null, 1, false);
        return SCRIPT_CONTINUE;
    }
    public int npeSetName(obj_id self, dictionary params)
    {
        setName(self, "Car'das Guard");
        return SCRIPT_CONTINUE;
    }
    public int OnPreloadComplete(obj_id self)
    {
        boolean setting = utils.checkConfigFlag("ScriptFlags", "npeSequencersActive");
        if (setting == true)
        {
            messageTo(self, "doEvents", null, 60, false);
        }
        return SCRIPT_CONTINUE;
    }
}
