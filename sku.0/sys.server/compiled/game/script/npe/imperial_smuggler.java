package script.npe;

import script.*;

import script.library.groundquests;
import script.library.chat;
import script.library.create;

public class imperial_smuggler extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        createTriggerVolume("npe_imperial_radius", 3.0f, true);
        return SCRIPT_CONTINUE;
    }
    public int OnTriggerVolumeEntered(obj_id self, String name, obj_id player)
    {
        if (!isPlayer(player))
        {
            return SCRIPT_CONTINUE;
        }
        if (groundquests.isTaskActive(player, "npe_smuggler_try", "impTalks"))
        {
            chat.chat(self, "Hey you! Get over here!");
            groundquests.sendSignal(player, "impTalks");
        }
        else 
        {
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_CONTINUE;
    }
    public int help(obj_id self, dictionary params)
    {
        location me = getLocation(self);
        me.setX (me.getX() + 1);
        me.setY (me.getY() + 1);
        create.object("npe_stormtrooper_quest", me);
        me.setX (me.getX() + 2);
        me.setY (me.getY() + 2);
        create.object("npe_stormtrooper_quest", me);
        return SCRIPT_CONTINUE;
    }
}
