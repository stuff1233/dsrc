package script.npe;

import script.*;
import script.library.weapons;

public class weak_dungeon extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        obj_id weapon = aiGetPrimaryWeapon(self);
        setWeaponMinDamage(weapon, 30);
        setWeaponMaxDamage(weapon, 60);
        weapons.setWeaponData(weapon);
        return SCRIPT_CONTINUE;
    }
    public int OnAttach(obj_id self)
    {
        obj_id weapon = aiGetPrimaryWeapon(self);
        setWeaponMinDamage(weapon, 30);
        setWeaponMaxDamage(weapon, 60);
        weapons.setWeaponData(weapon);
        return SCRIPT_CONTINUE;
    }
}
