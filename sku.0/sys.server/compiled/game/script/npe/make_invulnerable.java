package script.npe;

import script.*;

public class make_invulnerable extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        setInvulnerable(self, true);
        return SCRIPT_CONTINUE;
    }
}
