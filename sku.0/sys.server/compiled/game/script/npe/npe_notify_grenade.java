package script.npe;

import script.*;
import script.library.npe;
import script.library.groundquests;

public class npe_notify_grenade extends script.base_script
{
    public int OnReceivedItem(obj_id self, obj_id srcContainer, obj_id transferer, obj_id item)
    {
        if (isPlayer(item))
        {
            if (groundquests.isTaskActive(item, "npe_commando", "thugs"))
            {
                npe.giveGrenadePopUp(item, null);
            }
        }
        return SCRIPT_CONTINUE;
    }
}
