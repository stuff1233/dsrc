package script.npe;

import script.*;

public class npe_falcon_player extends script.base_script
{
    public int OnNewbieTutorialResponse(obj_id self, String strAction)
    {
        if (strAction.equals("clientReady"))
        {
            obj_id objShip = getTopMostContainer(self);
            messageTo(objShip, "doEvents", null, 1, false);
        }
        return SCRIPT_CONTINUE;
    }
}
