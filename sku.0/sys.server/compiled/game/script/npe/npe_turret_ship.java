package script.npe;

import script.*;

import script.library.utils;

public class npe_turret_ship extends script.base_script
{
    public int OnDestroy(obj_id self)
    {
        messageTo(utils.getObjIdScriptVar(self, "objParent"), "shipDied", null, 0, true);
        return SCRIPT_CONTINUE;
    }
}
