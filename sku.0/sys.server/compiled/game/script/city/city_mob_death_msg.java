package script.city;

import script.dictionary;
import script.obj_id;

public class city_mob_death_msg extends script.base_script
{
    public city_mob_death_msg()
    {
    }
    public int OnDestroy(obj_id self)
    {
        dictionary gangsta = new dictionary();
        gangsta.put("deadNpc", self);
        messageTo(getObjIdObjVar(self, "cityMobSpawner"), "cityMobKilled", gangsta, 1800, false);
        return SCRIPT_CONTINUE;
    }
}
