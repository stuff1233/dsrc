package script.city;

import script.*;

public class rebel_ship_spawner extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        createObject("object/static/structure/general/distant_ship_controller_rebel.iff", getLocation(self));
        return SCRIPT_CONTINUE;
    }
}
