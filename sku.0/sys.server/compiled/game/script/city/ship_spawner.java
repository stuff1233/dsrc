package script.city;

import script.obj_id;

public class ship_spawner extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        createObject("object/static/structure/general/distant_ship_controller2.iff", getLocation(self));
        return SCRIPT_CONTINUE;
    }
}
