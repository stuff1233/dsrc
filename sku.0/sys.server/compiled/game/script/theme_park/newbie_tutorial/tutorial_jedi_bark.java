package script.theme_park.newbie_tutorial;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.chat;

public class tutorial_jedi_bark extends script.base_script
{
    public tutorial_jedi_bark()
    {
    }
    public static final String JEDI_BARK = "newbie_tutorial/newbie_convo";
    public int OnEnteredCombat(obj_id self)
    {
        string_id bark = pickRandomBarkText();
        dictionary webster = new dictionary();
        webster.put("bark", bark);
        messageTo(self, "barkRandomPhrase", webster, 5.0f, true);
        return SCRIPT_CONTINUE;
    }
    public int barkRandomPhrase(obj_id self, dictionary params)
    {
        string_id bark = params.getStringId("bark");
        chat.chat(self, bark);
        string_id newBark = pickRandomBarkText();
        dictionary webster = new dictionary();
        webster.put("bark", newBark);
        messageTo(self, "barkRandomPhrase", webster, 25.0f, true);
        return SCRIPT_CONTINUE;
    }
    public string_id pickRandomBarkText()
    {
        return new string_id(JEDI_BARK, "jedi_bark" + rand(1,5));
    }
}
