package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.ai_lib;

public class exar_professor extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        setCreatureCoverVisibility(self, false);
        setInvulnerable(self, true);
        return SCRIPT_CONTINUE;
    }
    public int assume_death(obj_id self, dictionary params)
    {
        ai_lib.setMood(self, "npc_dead_01");
        return SCRIPT_CONTINUE;
    }
}
