package script.theme_park.heroic.exar_kun;

import script.*;
import script.library.trial;

public class open_cultist extends script.base_script
{
    public int activateSelf(obj_id self, dictionary params)
    {
        setInvulnerable(self, false);
        obj_id target = trial.getClosest(self, trial.getValidTargetsInCell(trial.getTop(self), "r2"));
        startCombat(self, target);
        return SCRIPT_CONTINUE;
    }
}
