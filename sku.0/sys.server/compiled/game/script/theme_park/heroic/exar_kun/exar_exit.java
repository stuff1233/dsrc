package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.instance;

public class exar_exit extends script.base_script
{
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info item)
    {
        if (getDistance(player, self) <= 6.0f)
        {
            item.addRootMenu(menu_info_types.ITEM_USE, new string_id("item_n", "heroic_exar_exit"));
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item)
    {
        if (item == menu_info_types.ITEM_USE)
        {
            instance.requestExitPlayer("heroic_exar_kun", player);
        }
        return SCRIPT_CONTINUE;
    }
}
