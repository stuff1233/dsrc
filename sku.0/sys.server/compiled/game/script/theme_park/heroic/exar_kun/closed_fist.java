package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.buff;
import script.library.trial;

public class closed_fist extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        trial.setHp(self, trial.HP_EXAR_CLOSED);
        return SCRIPT_CONTINUE;
    }
    public int OnEnteredCombat(obj_id self)
    {
        buff.applyBuff(self, "closed_balance_buff");
        return SCRIPT_CONTINUE;
    }
}
