package script.conversation;

import script.library.*;
import script.*;

public class barn_ranchhand extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "barn_ranchhand";

    private boolean barn_ranchhand_condition_isBuildingOwner(obj_id player, obj_id npc) {
        obj_id building = getTopMostContainer(npc);
        if (isIdValid(building)) {
            obj_id owner = getOwner(building);
            if (isIdValid(owner) && owner == player) {
                return true;
            }
        }
        return false;
    }

    private boolean barn_ranchhand_condition_playerHasBeasts(obj_id player) {
        return beast_lib.getTotalBeastControlDevices(player) > 0;
    }

    private boolean barn_ranchhand_condition_ranchhandHasBeasts(obj_id npc) {
        return tcg.getTotalBarnStoredBeastsFromRanchhand(npc) > 0;
    }

    private boolean barn_ranchhand_condition_noBeasts(obj_id player, obj_id npc) {
        return !(beast_lib.getTotalBeastControlDevices(player) < 0 || tcg.getTotalBarnStoredBeastsFromRanchhand(npc) > 0);
    }

    private boolean barn_ranchhand_condition_barnIsFull(obj_id npc) {
        return tcg.getTotalBarnStoredBeastsFromRanchhand(npc) >= tcg.MAX_NUM_BARN_PETS;
    }

    private boolean barn_ranchhand_condition_playerIsFull(obj_id player) {
        return callable.hasMaxStoredCombatPets(player);
    }

    private void barn_ranchhand_action_storeBeast(obj_id player, obj_id npc) {
        tcg.barnStoreBeastPrompt(player, npc);
    }

    private void barn_ranchhand_action_reclaimBeast(obj_id player, obj_id npc) {
        obj_id barn = getTopMostContainer(npc);
        if (isIdValid(barn)) {
            tcg.barnReclaimBeastPrompt(player, barn, npc);
        }
    }

    private void barn_ranchhand_action_displayBeast(obj_id player, obj_id npc) {
        obj_id barn = getTopMostContainer(npc);
        if (isIdValid(barn)) {
            tcg.barnDisplayBeastPrompt(player, barn, npc);
        }
    }

    private String barn_ranchhand_tokenTO_barnOwnerName(obj_id player, obj_id npc) {
        String name = getGender(player) == GENDER_MALE ? "Sir" : "Ma'am";
        obj_id building = getTopMostContainer(npc);
        if (isIdValid(building)) {
            String buildingOwnerName = getStringObjVar(building, player_structure.VAR_OWNER);
            if (buildingOwnerName != null && buildingOwnerName.length() > 0) {
                name = toUpper(buildingOwnerName, 0);
            }
        }
        return name;
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (barn_ranchhand_condition_isBuildingOwner(player, self)) {
            String name = barn_ranchhand_tokenTO_barnOwnerName(player, self);
            if (barn_ranchhand_condition_playerHasBeasts(player)) {
                return OnStartNpcConversation(SCRIPT, "s_4", "s_6", player, self);
            }
            if (barn_ranchhand_condition_ranchhandHasBeasts(self)) {
                return OnStartNpcConversation(SCRIPT, "s_4", new String[] {"s_11", "s_19"}, player, self);
            }
            if (barn_ranchhand_condition_noBeasts(player, self)) {
                return OnStartNpcConversation(SCRIPT, "s_4", "s_23", player, self);
            }
        }
        return OnStartNpcConversation(SCRIPT, "s_22", "s_26", player, self);
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_6":
                if (barn_ranchhand_condition_barnIsFull(self)) {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_27"));
                } else {
                    barn_ranchhand_action_storeBeast(player, self);
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_9"));
                }
                break;
            case "s_11":
                if (barn_ranchhand_condition_playerIsFull(player)) {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_28"));
                } else {
                    barn_ranchhand_action_reclaimBeast(player, self);
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_14"));
                }
                break;
            case "s_19":
                barn_ranchhand_action_displayBeast(player, self);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_20"));
                break;
            case "s_23":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_24"));
                break;
            case "s_26":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_30"));
                break;
        }
    }
}
