package script.conversation;

import script.*;
import script.library.ai_lib;
import script.library.chat;
import script.library.smuggler;
import script.library.utils;

import java.util.ArrayList;
import java.util.List;

public class aurilia_junk_dealer extends script.conversation.base.conversation_base {
    public static final String SCRIPT = "conversation/aurilia_junk_dealer";

    private void startDealing(obj_id player, obj_id npc) {
        dictionary params = new dictionary();
        params.put("player", player);
        messageTo(npc, "startDealing", params, 1.0f, false);
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        List<String> responses = new ArrayList<>();
        if (smuggler.checkInventory(player, self)) {
            responses.add("s_c86eba88");
        }
        responses.add("s_370a03c");
        return OnStartNpcConversation(SCRIPT, "s_3c06418f", responses, player, self);
    }
    
    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_c86eba88":
                startDealing(player, self);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_24f30320"));
                break;
            case "s_370a03c":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_df5bd64e"));
                break;
        }
    }
}
