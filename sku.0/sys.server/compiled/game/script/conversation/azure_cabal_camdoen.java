package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class azure_cabal_camdoen extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/azure_cabal_camdoen";

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_dm2");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_dml1");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_dml2");
    }

    private boolean azure_cabal_camdoen_condition_onTask_30ENF_Q1_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_wfs_quest_complete");
    }

    public boolean azure_cabal_camdoen_condition_onTask_30ENF_Repeater_Q1orQ2(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_camdoen_01") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_camdoen_02"));
    }

    private boolean azure_cabal_camdoen_condition_onQuest_30ENF_Q1(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q2A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_dm1");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q2B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_dm2");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q2C(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_dm3") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_dm3_retrieve"));
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q2D(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_retrieve_01") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_dml1"));
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_dm1");
    }

    private boolean azure_cabal_camdoen_condition_onTask_30ENF_Q2_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_wfs_quest_complete");
    }

    private boolean azure_cabal_camdoen_condition_onQuest_30ENF_Q2(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02");
    }

    private boolean azure_cabal_camdoen_condition_onTask_30ENF_Repeater_Q1(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_camdoen_01");
    }

    private boolean azure_cabal_camdoen_condition_onTask_30ENF_Repeater_Q2(obj_id player)
    {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_camdoen_02");
    }

    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q2D_inv_use(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_02", "ac_30_camdoen_02_retrieve_02");
    }
    
    private boolean azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_sacrifice_retrieve") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01", "ac_30_camdoen_01_sacrifice_enc"));
    }

    private void azure_cabal_camdoen_action_grant30Q1(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc3_camdoen_01");
    }

    private void azure_cabal_camdoen_action_signal_30ENF_ClearRepeaterQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_repeater_loruna_enf_30_signal_clear_quest");
    }

    private void azure_cabal_camdoen_action_grant30Q2(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc3_camdoen_02");
    }

    private void azure_cabal_camdoen_action_signal_30ENF_CompleteQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_quest_complete");
    }

    private void azure_cabal_camdoen_action_signal_30ENF_no_sacrifice(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.clearQuest(player, "ac_repeater_loruna_enf_30_dnpc3_camdoen_01");
        groundquests.grantQuest(player, "quest/ac_ongoing_launchpad_loruna_repeater");
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (ai_lib.isInCombat(self) || ai_lib.isInCombat(player)) {
            return SCRIPT_OVERRIDE;
        }
        if (azure_cabal_camdoen_condition_onTask_30ENF_Repeater_Q1(player)) {
            azure_cabal_camdoen_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_168", new String[] {"s_170", "s_171"}, player, self);
        }
        if (azure_cabal_camdoen_condition_onTask_30ENF_Repeater_Q2(player)) {
            azure_cabal_camdoen_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_174", new String[] {"s_175", "s_176"}, player, self);
        }
        if (azure_cabal_camdoen_condition_onTask_30ENF_Q1_Return(player)) {
            azure_cabal_camdoen_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_189", "s_196", player, self);
        }
        if (azure_cabal_camdoen_condition_onQuest_30ENF_Q1(player)) {
            return OnStartNpcConversation(SCRIPT, "s_192", new String[] {"s_193", "s_194"}, player, self);
        }
        if (azure_cabal_camdoen_condition_onTask_30ENF_Q2_Return(player)) {
            azure_cabal_camdoen_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_191", "s_200", player, self);
        }
        if (azure_cabal_camdoen_condition_onQuest_30ENF_Q2(player)) {
            return OnStartNpcConversation(SCRIPT, "s_282", new String[] {"s_283", "s_356"}, player, self);
        }
        chat.chat(self, player, new string_id(SCRIPT, "s_359"));
        return SCRIPT_CONTINUE;
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_108":
                setupResponses(SCRIPT, "s_110", new String[] {"s_111", "s_112"}, player);
                break;
            case "s_109":
            case "s_112":
            case "s_118":
                setupResponses(SCRIPT, "s_120", new String[] {"s_121", "s_123"}, player);
                break;
            case "s_111":
                setupResponses(SCRIPT, "s_113", new String[] {"s_114", "s_118"}, player);
                break;
            case "s_114":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_339"));
                break;
            case "s_121":
                setupResponses(SCRIPT, "s_122", new String[] {"s_124", "s_125"}, player);
                break;
            case "s_124":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_126"));
                break;
            case "s_170":
            case "s_182":
                azure_cabal_camdoen_action_grant30Q1(player, self);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_172"));
                break;
            case "s_171":
                setupResponses(SCRIPT, "s_181", new String[] {"s_182", "s_183"}, player);
                break;
            case "s_183":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_185"));
                break;
            case "s_175":
            case "s_178":
                azure_cabal_camdoen_action_grant30Q2(player, self);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_180"));
                break;
            case "s_176":
                setupResponses(SCRIPT, "s_177", new String[] {"s_178", "s_179"}, player);
                break;
            case "s_179":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_186"));
                break;
            case "s_123":
            case "s_125":
            case "s_193":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] {"s_216", "s_217"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] {"s_239", "s_240"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] {"s_108", "s_109"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] {"s_87", "s_106"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(SCRIPT, "s_299", new String[] {"s_301", "s_303"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_194":
            case "s_233":
            case "s_235":
            case "s_244":
            case "s_246":
            case "s_249":
            case "s_251":
            case "s_255":
            case "s_304":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_195"));
                break;
            case "196":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_198"));
                break;
            case "s_216":
                setupResponses(SCRIPT, "s_218", new String[] {"s_220", "s_221"}, player);
                break;
            case "s_217":
                setupResponses(SCRIPT, "s_219", new String[] {"s_228", "s_227"}, player);
                break;
            case "s_220":
                setupResponses(SCRIPT, "s_222", new String[] {"s_223", "s_224"}, player);
                /*
                ** Code simplification has shown this is now unreachable - leaving it here for future reference if this is not supposed
                ** to be the case.  It's believeable that it's meant to be skipped, but also looks like it could have been a bug.
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(new String[] {"s_199", "s_239", "s_240"});
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(new String[] {"s_201", "s_108", "s_109"});
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(new String[] {"s_203", "s_87", "s_106"});
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(new String[] {"s_299", "s_301", "s_303"});
                } else {
                    npcEndConversationWithMessage("s_167");
                }
                */
                break;
            case "s_223":
                setupResponses(SCRIPT, "s_225", "s_237", player);
                break;
            case "s_224":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] {"s_216", "s_217"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] {"s_239", "s_240"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] {"s_108", "s_109"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] {"s_87", "s_106"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(SCRIPT, "s_299", new String[] {"s_301", "s_303"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_237":
                setupResponses(SCRIPT, "s_219", new String[] {"s_228", "s_227"}, player);
                break;
            case "s_228":
                setupResponses(SCRIPT, "s_229", new String[] {"s_228", "s_227", "s_233"}, player);
                break;
            case "s_227":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] {"s_216", "s_217"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] {"s_239", "s_240"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] {"s_108", "s_109"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] {"s_87", "s_106"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(SCRIPT, "s_299", new String[] {"s_301", "s_303"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_230":
                setupResponses(SCRIPT, "s_232", new String[] {"s_235", "s_236"}, player);
                break;
            case "s_231":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] {"s_216", "s_217"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] {"s_239", "s_240"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] {"s_108", "s_109"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] {"s_87", "s_106"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(SCRIPT, "s_299", new String[] {"s_301", "s_303"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_236":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] {"s_216", "s_217"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] {"s_239", "s_240"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] {"s_108", "s_109"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] {"s_87", "s_106"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q1E(player)) {
                    setupResponses(SCRIPT, "s_299", new String[] {"s_301", "s_303"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_239":
                setupResponses(SCRIPT, "s_241", new String[] {"s_243", "s_252", "s_244"}, player);
                break;
            case "s_240":
            case "s_252":
            case "s_254":
                setupResponses(SCRIPT, "s_242", new String[] {"s_245", "s_246"}, player);
                break;
            case "s_243":
                setupResponses(SCRIPT, "s_253", new String[] {"s_254", "s_255"}, player);
                break;
            case "s_245":
                setupResponses(SCRIPT, "s_247", new String[] {"s_248", "s_249"}, player);
                break;
            case "s_248":
                setupResponses(SCRIPT, "s_250", "s_251", player);
                break;
            case "s_306":
                if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q2A(player)) {
                    setupResponses(SCRIPT, "s_358", new String[] {"s_360", "s_384", "s_403"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q2B(player)) {
                    setupResponses(SCRIPT, "s_286", new String[] {"s_288", "s_312", "s_401"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q2C(player)) {
                    setupResponses(SCRIPT, "s_396", new String[] {"s_399", "s_400", "s_402"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q2D(player)) {
                    setupResponses(SCRIPT, "s_398", new String[] {"s_417", "s_418", "s_419"}, player);
                } else if (azure_cabal_camdoen_condition_onAssignment_30ENF_Q2D_inv_use(player)) {
                    faceTo(self, player);
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_352"));
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_354"));
                }
                break;
        }
    }
}
