package script.conversation;

import script.library.*;
import script.*;

public class aurilia_rohak extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/aurilia_rohak";

    private boolean aurilia_rohak_condition_axkva_min_intro_01(obj_id player) {
        return groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_01");
    }

    private boolean aurilia_rohak_condition_axkva_min_intro_02(obj_id player) {
        return groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_02");
    }

    private boolean aurilia_rohak_condition_axkva_min_intro_04(obj_id player) {
        return groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_04");
    }

    private boolean aurilia_rohak_condition_axkva_min_intro_06(obj_id player) {
        return groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_06");
    }

    private boolean aurilia_rohak_condition_notCompletedTokenBox(obj_id player) {
        return !groundquests.isQuestActiveOrComplete(player, "rohak_token_box");
    }

    private void aurilia_rohak_action_axkva_min_intro_02_signal(obj_id player) {
        groundquests.sendSignal(player, "axkva_min_intro_02");
    }

    private void aurilia_rohak_action_axkva_min_intro_04_signal(obj_id player) {
        groundquests.sendSignal(player, "axkva_min_intro_04");
    }

    private void aurilia_rohak_action_axkva_min_intro_06_signal(obj_id player) {
        String axkvaMinShardsString = "item_axkva_min_shards_04_01";
        obj_id inv = getObjectInSlot(player, "inventory");
        if (isIdValid(inv)) {
            obj_id axkvaMinShard = static_item.createNewItemFunction(axkvaMinShardsString, inv);
            prose_package pp = new prose_package("quest/ground/system_message", "placed_in_inventory_plural");
            pp.setTO(getNameStringId(axkvaMinShard));
            sendSystemMessageProse(player, pp);
            groundquests.sendSignal(player, "axkva_min_intro_06");
        }
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (aurilia_rohak_condition_axkva_min_intro_06(player)) {
            aurilia_rohak_action_axkva_min_intro_06_signal(player);
            return OnStartNpcConversation(SCRIPT, "s_9", "s_25", player, self);
        } else if (aurilia_rohak_condition_axkva_min_intro_04(player)) {
            doAnimationAction(self, "explain");
            return OnStartNpcConversation(SCRIPT, "s_10", "s_32", player, self);
        } else if (aurilia_rohak_condition_notCompletedTokenBox(player)) {
            if (aurilia_rohak_condition_axkva_min_intro_01(player)){
                return OnStartNpcConversation(SCRIPT, "s_23", new String[]{"s_26", "s_41", "s_37"}, player, self);
            } else if (aurilia_rohak_condition_axkva_min_intro_02(player)) {
                return OnStartNpcConversation(SCRIPT, "s_23", new String[]{"s_26", "s_41", "s_35"}, player, self);
            } else {
                return OnStartNpcConversation(SCRIPT, "s_23", new String[]{"s_26", "s_41"}, player, self);
            }
        } else{
            if (aurilia_rohak_condition_axkva_min_intro_01(player)) {
                return OnStartNpcConversation(SCRIPT, "s_18", "s_24", player, self);
            } else if (aurilia_rohak_condition_axkva_min_intro_02(player)) {
                return OnStartNpcConversation(SCRIPT, "s_18", "s_28", player, self);
            }
        }
        chat.chat(self, player, new string_id(SCRIPT, "s_18"));
        return SCRIPT_DEFAULT;
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_25":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_27"));
                break;
            case "s_32":
                setupResponses(SCRIPT, "s_34", "s_36", player);
                break;
            case "s_26":
                groundquests.grantQuest(player, "rohak_token_box");
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_39"));
                break;
            case "s_41":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_43"));
                break;
            case "s_35":
            case "s_28":
                setupResponses(SCRIPT, "s_29", "s_31", player);
                break;
            case "s_31":
                doAnimationAction(self, "rub_chin_thoughtful");
                aurilia_rohak_action_axkva_min_intro_02_signal(player);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_33"));
                break;
            case "s_36":
                setupResponses(SCRIPT, "s_38", "s_40", player);
                break;
            case "s_40":
                aurilia_rohak_action_axkva_min_intro_04_signal(player);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_42"));
                break;
            case "s_24":
            case "s_37":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_30"));
                break;
        }
    }
}
