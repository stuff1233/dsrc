package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class azure_cabal_haadj extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/azure_cabal_haadj";

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q1B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01", "ac_30_haadj_01_dm2");
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q1C(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01", "ac_30_haadj_01_dml1");
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q1D(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01", "ac_30_haadj_01_dml2");
    }

    private boolean azure_cabal_haadj_condition_onTask_30ENF_Q1_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01", "ac_30_haadj_01_wfs_quest_complete");
    }

    public boolean azure_cabal_haadj_condition_onTask_30ENF_Repeater_Q1orQ2(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_haadj_01") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_haadj_02"));
    }

    private boolean azure_cabal_haadj_condition_onQuest_30ENF_Q1(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01");
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q2A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dm1");
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q2B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dm2");
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q2C(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dml1") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dml1_retrieve"));
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q2D(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dml2_retrieve") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_dml2"));
    }

    private boolean azure_cabal_haadj_condition_onAssignment_30ENF_Q1A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01", "ac_30_haadj_01_dm1");
    }

    private boolean azure_cabal_haadj_condition_onTask_30ENF_Q2_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02", "ac_30_haadj_02_wfs_quest_complete");
    }

    private boolean azure_cabal_haadj_condition_onQuest_30ENF_Q2(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02");
    }

    private boolean azure_cabal_haadj_condition_onTask_30ENF_Repeater_Q1(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_haadj_01");
    }

    private boolean azure_cabal_haadj_condition_onTask_30ENF_Repeater_Q2(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_haadj_02");
    }

    public boolean azure_cabal_haadj_condition_onQuest_30ENF_Q1orQ2(obj_id player) {
        return (groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_01") || groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc1_haadj_02"));
    }
    
    private void azure_cabal_haadj_action_grant30Q1(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc1_haadj_01");
    }

    private void azure_cabal_haadj_action_signal_30ENF_ClearRepeaterQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_repeater_loruna_enf_30_signal_clear_quest");
    }

    public void azure_cabal_haadj_action_facePlayer(obj_id player, obj_id npc) {
        faceTo(npc, player);
    }

    private void azure_cabal_haadj_action_grant30Q2(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc1_haadj_02");
    }

    private void azure_cabal_haadj_action_signal_30ENF_CompleteQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_quest_complete");
    }
    
    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (azure_cabal_haadj_condition_onTask_30ENF_Repeater_Q1(player)) {
            azure_cabal_haadj_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_168", new String[] {"s_170", "s_171"}, player, self);
        } else if (azure_cabal_haadj_condition_onTask_30ENF_Repeater_Q2(player)) {
            azure_cabal_haadj_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_174", new String[] {"s_175", "s_176"}, player, self);
        } else if (azure_cabal_haadj_condition_onTask_30ENF_Q1_Return(player)) {
            azure_cabal_haadj_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_189", new String[] {"s_196"}, player, self);
        } else if (azure_cabal_haadj_condition_onTask_30ENF_Q2_Return(player)) {
            azure_cabal_haadj_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_191", new String[] {"s_200"}, player, self);
        } else if (azure_cabal_haadj_condition_onQuest_30ENF_Q1(player)) {
            return OnStartNpcConversation(SCRIPT, "s_192", new String[] {"s_193", "s_194"}, player, self);
        } else if (azure_cabal_haadj_condition_onQuest_30ENF_Q2(player)) {
            return OnStartNpcConversation(SCRIPT, "s_282", new String[] {"s_283", "s_328"}, player, self);
        } else {
            chat.chat(player, new string_id(SCRIPT, "s_330"));
        }
        return SCRIPT_CONTINUE;
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        // TODO: Implement this
    }
}
