package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.obj_id;
import script.string_id;

public class aurilia_mellichae extends script.conversation.base.conversation_base {
    public static final String SCRIPT = "conversation/aurilia_mellichae";

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        doAnimationAction(self, "standing_raise_fist");
        chat.chat(player, new string_id(SCRIPT, "s_4"));
        return SCRIPT_CONTINUE;
    }
}
