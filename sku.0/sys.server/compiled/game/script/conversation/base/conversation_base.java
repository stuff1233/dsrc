package script.conversation.base;

import script.*;
import script.library.ai_lib;

import script.library.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class conversation_base extends script.base_script {
    public static int OnAttach(obj_id npc) {
        setCondition(npc, CONDITION_CONVERSABLE);
        return SCRIPT_CONTINUE;
    }

    public static int OnObjectMenuRequest(obj_id self, obj_id player, menu_info menuInfo) {
        int menu = menuInfo.addRootMenu(menu_info_types.CONVERSE_START, null);
        menu_info_data menuInfoData = menuInfo.getMenuItemById(menu);
        menuInfoData.setServerNotify(false);
        //setCondition(self, CONDITION_CONVERSABLE);
        return SCRIPT_CONTINUE;
    }

    public static int OnIncapacitated(obj_id npc, String conversation) {
        clearCondition(npc, CONDITION_CONVERSABLE);
        detachScript(npc, conversation);
        return SCRIPT_CONTINUE;
    }

    public static int OnStartNpcConversation(String c_stringFile, String index, List<String> responses, obj_id player, obj_id npc) {
        return OnStartNpcConversation(c_stringFile, index, responses.toArray(new String[responses.size()]), player, npc);
    }

    public static int OnStartNpcConversation(String c_stringFile, String index, String[] responses, obj_id player, obj_id npc) {
        if (!ai_lib.isInCombat(npc) && !ai_lib.isInCombat(player)) {
            prose_package pp = new prose_package(c_stringFile, index);
            pp.setTT(player);
            faceTo(npc, player);
            npcStartConversation(player, npc, c_stringFile, null, pp, convertToStringIds(c_stringFile, responses));
        }
        return SCRIPT_CONTINUE;
    }

    public static int OnStartNpcConversation(String c_stringFile, String index, String response, obj_id player, obj_id npc) {
        return OnStartNpcConversation(c_stringFile, index, new String[]{response}, player, npc);
    }

    public static void setupResponses(String c_stringFile, String index, String[] responses, obj_id player) {
        npcSpeak(player, new string_id(c_stringFile, index));
        npcSetConversationResponses(player, convertToStringIds(c_stringFile, responses));
    }

    public static void setupResponses(String c_stringFile, String index, String response, obj_id player) {
        setupResponses(c_stringFile, index, new String[]{ response }, player);
    }

    public static void setupResponses(String c_stringFile, String index, List<String> responses, obj_id player) {
        setupResponses(c_stringFile, index, responses.toArray(new String[responses.size()]), player);
    }

    public static string_id[] convertToStringIds(String c_stringFile, List<String> strings) {
        return convertToStringIds(c_stringFile, strings.toArray(new String[strings.size()]));
    }

    public static string_id[] convertToStringIds(String c_stringFile, String[] strings) {
        string_id[] arr = new string_id[strings.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new string_id(c_stringFile, strings[i]);
        }
        return arr;
    }

    public static void showTokenVendorUI(obj_id player, obj_id npc) {
        dictionary d = new dictionary();
        d.put("player", player);
        messageTo(npc, "showInventorySUI", d, 0, false);
    }
}
