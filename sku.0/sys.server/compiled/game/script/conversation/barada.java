package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;

import script.library.utils;
import script.*;

public class barada extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/barada";

    private boolean barada_condition_completedBarada(obj_id player) {
        return groundquests.hasCompletedQuest(player, "quest/jabba_barada_v2");
    }

    private boolean barada_condition_completedPorcellus(obj_id player) {
        return groundquests.hasCompletedQuest(player, "quest/jabba_porcellus");
    }

    private boolean barada_condition_findingParts(obj_id player) {
        return groundquests.isTaskActive(player, "quest/jabba_barada_v2", "suspiciousParts");
    }

    private boolean barada_condition_killingCompetition(obj_id player) {
        return groundquests.isTaskActive(player, "quest/jabba_barada_v2", "PodRaceVillainy");
    }

    private boolean barada_condition_readyToReturn(obj_id player) {
        return groundquests.isTaskActive(player, "quest/jabba_barada_v2", "readyToReturn");
    }
    
    private void barada_action_grantBaradaQuest(obj_id player) {
        groundquests.requestGrantQuest(player, "quest/jabba_barada_v2");
    }

    private void barada_action_sendBaradaDoneSignal(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "doneWithBarada");
        groundquests.grantQuest(player, "pointer_bib_fortuna");
    }

    private void barada_action_clearPointer(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "found_barada");
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (barada_condition_completedBarada(player)) {
            chat.chat(self, player, new string_id(SCRIPT, "s_4"));
        } else if (barada_condition_readyToReturn(player)) {
            barada_action_sendBaradaDoneSignal(player, self);
            chat.chat(self, player, new string_id(SCRIPT, "23"));
        } else if (barada_condition_killingCompetition(player)) {
            chat.chat(self, player, new string_id(SCRIPT, "20"));
        } else if (barada_condition_findingParts(player)) {
            chat.chat(self, player, new string_id(SCRIPT, "19"));
        } else if (barada_condition_completedPorcellus(player)) {
            barada_action_clearPointer(player, self);
            return OnStartNpcConversation(SCRIPT, "s_11", new String[]{ "s_13", "s_40" }, player, self);
        } else {
            chat.chat(self, player, new string_id(SCRIPT, "s_44"));
        }
        return SCRIPT_CONTINUE;
    }

    public void OnNpcConversationResponse(obj_id self, String conversationId, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_13":
                setupResponses(SCRIPT, "s_15", new String[] { "s_17", "s_36"}, player);
                break;
            case "s_32":
            case "s_40":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_42"));
                break;
            case "s_17":
                setupResponses(SCRIPT, "s_24", new String[] { "s_26", "s_34"}, player);
                break;
            case "s_30":
            case "s_34":
            case "s_36":
                barada_action_grantBaradaQuest(player);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_38"));
                break;
            case "s_26":
                setupResponses(SCRIPT, "s_28", new String[] { "s_30", "s_32"}, player);
                break;
        }
    }
}
