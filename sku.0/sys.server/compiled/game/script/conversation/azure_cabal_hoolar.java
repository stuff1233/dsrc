package script.conversation;

import script.*;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;

public class azure_cabal_hoolar extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/azure_cabal_hoolar";

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q1B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01", "ac_30_hoolar_01_dm2");
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q1C(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01", "ac_30_hoolar_01_dml1");
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q1D(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01", "ac_30_hoolar_01_dml2");
    }

    private boolean azure_cabal_hoolar_condition_onTask_30ENF_Q1_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01", "ac_30_hoolar_01_wfs_quest_complete");
    }

    public boolean azure_cabal_hoolar_condition_onTask_30ENF_Repeater_Q1orQ2(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_hoolar_01") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_hoolar_02"));
    }

    private boolean azure_cabal_hoolar_condition_onQuest_30ENF_Q1(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01");
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q2A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dm1");
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q2B(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dm2");
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q2C(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dml1") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dml1_retrieve"));
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q2D(obj_id player) {
        return (groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dml2_retrieve") || groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_dml2"));
    }

    private boolean azure_cabal_hoolar_condition_onAssignment_30ENF_Q1A(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01", "ac_30_hoolar_01_dm1");
    }

    private boolean azure_cabal_hoolar_condition_onTask_30ENF_Q2_Return(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02", "ac_30_hoolar_02_wfs_quest_complete");
    }

    private boolean azure_cabal_hoolar_condition_onQuest_30ENF_Q2(obj_id player) {
        return groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02");
    }
    
    private boolean azure_cabal_hoolar_condition_onTask_30ENF_Repeater_Q1(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_hoolar_01");
    }
    
    private boolean azure_cabal_hoolar_condition_onTask_30ENF_Repeater_Q2(obj_id player) {
        return groundquests.isTaskActive(player, "ac_repeater_loruna_enf_30", "ac_repeater_loruna_enf_30_WFS_hoolar_02");
    }

    public boolean azure_cabal_hoolar_condition_onQuest_30ENF_Q1orQ2(obj_id player) {
        return (groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_01") || groundquests.isQuestActive(player, "ac_repeater_loruna_enf_30_dnpc2_hoolar_02"));
    }

    private void azure_cabal_hoolar_action_grant30Q1(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc2_hoolar_01");
    }

    private void azure_cabal_hoolar_action_signal_30ENF_ClearRepeaterQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_repeater_loruna_enf_30_signal_clear_quest");
    }

    private void azure_cabal_hoolar_action_grant30Q2(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.grantQuest(player, "quest/ac_repeater_loruna_enf_30_dnpc2_hoolar_02");
    }

    private void azure_cabal_hoolar_action_signal_30ENF_CompleteQuest(obj_id player, obj_id npc) {
        faceTo(npc, player);
        groundquests.sendSignal(player, "ac_quest_complete");
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (azure_cabal_hoolar_condition_onTask_30ENF_Repeater_Q1(player)) {
            azure_cabal_hoolar_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_168", new String[] { "s_170", "s_171" }, player, self);
        }

        if (azure_cabal_hoolar_condition_onTask_30ENF_Repeater_Q2(player)) {
            azure_cabal_hoolar_action_signal_30ENF_ClearRepeaterQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_174", new String[] { "s_175", "s_176" }, player, self);
        }

        if (azure_cabal_hoolar_condition_onTask_30ENF_Q1_Return(player)) {
            azure_cabal_hoolar_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_189", "s_196", player, self);
        }

        if (azure_cabal_hoolar_condition_onTask_30ENF_Q2_Return(player)) {
            azure_cabal_hoolar_action_signal_30ENF_CompleteQuest(player, self);
            return OnStartNpcConversation(SCRIPT, "s_191", "s_200", player, self);
        }

        if (azure_cabal_hoolar_condition_onQuest_30ENF_Q1(player)) {
            return OnStartNpcConversation(SCRIPT, "s_192", new String[] { "s_193", "s_194" }, player, self);
        }

        if (azure_cabal_hoolar_condition_onQuest_30ENF_Q2(player)) {
            return OnStartNpcConversation(SCRIPT, "s_282", new String[] { "s_283", "s_328"}, player, self);
        }
        chat.chat(self, new string_id(SCRIPT, "s_330"));
        return SCRIPT_CONTINUE;
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_170":
            case "s_182":
                azure_cabal_hoolar_action_grant30Q1(player, self);
                npcEndConversationWithMessage(self, new string_id(SCRIPT, "s_172"));
                break;
            case "s_171":
                setupResponses(SCRIPT, "s_181", new String[] { "s_182", "s_183"}, player);
                break;
            case "s_175":
            case "s_178":
                azure_cabal_hoolar_action_grant30Q2(player, self);
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_180"));
                break;
            case "s_176":
                setupResponses(SCRIPT, "s_177", new String[] { "s_178", "s_179"}, player);
                break;
            case "s_196":
            case "s_200":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_198"));
                break;
            case "s_193":
            case "s_221":
            case "s_162":
            case "s_164":
            case "s_125":
            case "s_123":
            case "s_236":
            case "s_231":
            case "s_227":
            case "s_224":
                if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q1A(player)) {
                    setupResponses(SCRIPT, "s_197", new String[] { "s_216", "s_217"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q1B(player)) {
                    setupResponses(SCRIPT, "s_199", new String[] { "s_239", "s_240"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q1C(player)) {
                    setupResponses(SCRIPT, "s_201", new String[] { "s_108", "s_109"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q1D(player)) {
                    setupResponses(SCRIPT, "s_203", new String[] { "s_87", "s_106"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_283":
                if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2A(player)) {
                    setupResponses(SCRIPT, "s_358", new String[] { "s_384", "s_403"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2B(player)) {
                    setupResponses(SCRIPT, "s_286", new String[] { "s_288", "s_312", "s_401"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2C(player)) {
                    setupResponses(SCRIPT, "s_396", new String[] { "s_399", "s_400", "s_402"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2D(player)) {
                    setupResponses(SCRIPT, "s_398", new String[] { "s_417", "s_418", "s_419"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_194":
            case "s_328":
            case "s_433":
            case "s_425":
            case "s_431":
            case "s_428":
            case "s_422":
            case "s_419":
            case "s_416":
            case "s_414":
            case "s_409":
            case "s_402":
            case "s_316":
            case "s_304":
            case "s_298":
            case "s_233":
            case "s_235":
            case "s_292":
            case "s_401":
            case "s_394":
            case "s_388":
            case "s_376":
            case "s_370":
            case "s_364":
            case "s_403":
            case "s_251":
            case "s_249":
            case "s_246":
            case "s_255":
            case "s_244":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_195"));
                break;
            case "s_378":
            case "s_380":
            case "s_382":
            case "s_384":
                setupResponses(SCRIPT, "s_386", new String[] { "s_388", "s_390"}, player);
                break;
            case "s_427":
                setupResponses(SCRIPT, "s_429", new String[] { "s_430", "s_431"}, player);
                break;
            case "s_360":
                setupResponses(SCRIPT, "s_362", new String[] { "s_364", "s_366", "s_382"}, player);
                break;
            case "s_424":
                setupResponses(SCRIPT, "s_426", new String[] { "s_427", "s_428"}, player);
                break;
            case "s_417":
                setupResponses(SCRIPT, "s_420", new String[] { "s_421", "s_422"}, player);
                break;
            case "s_418":
            case "s_421":
                setupResponses(SCRIPT, "s_423", new String[] { "s_424", "s_425"}, player);
                break;
            case "s_308":
            case "s_310":
            case "s_306":
                if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2A(player)) {
                    setupResponses(SCRIPT, "s_358", new String[] { "s_360", "s_384", "s_403"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2B(player)) {
                    setupResponses(SCRIPT, "s_286", new String[] { "s_288", "s_312", "s_401"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2C(player)) {
                    setupResponses(SCRIPT, "s_396", new String[] { "s_399", "s_400", "s_402"}, player);
                } else if (azure_cabal_hoolar_condition_onAssignment_30ENF_Q2D(player)) {
                    setupResponses(SCRIPT, "s_398", new String[] { "s_417", "s_418", "s_419"}, player);
                } else {
                    npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_167"));
                }
                break;
            case "s_318":
            case "s_288":
                setupResponses(SCRIPT, "s_290", new String[] { "s_292", "s_294", "s_310"}, player);
                break;
            case "s_366":
                setupResponses(SCRIPT, "s_368", new String[] { "s_370", "s_372", "s_380"}, player);
                break;
            case "s_400":
            case "s_407":
            case "s_410":
                setupResponses(SCRIPT, "s_411", new String[] { "s_413", "s_414"}, player);
                break;
            case "s_312":
                setupResponses(SCRIPT, "s_314", new String[] { "s_316", "s_318"}, player);
                break;
            case "s_179":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_186"));
                break;
            case "s_117":
            case "s_109":
            case "s_112":
            case "s_118":
                setupResponses(SCRIPT, "s_120", new String[] { "s_121", "s_123"}, player);
                break;
            case "s_106":
            case "s_104":
            case "s_102":
            case "s_139":
                setupResponses(SCRIPT, "s_127", new String[] { "s_132", "s_164"}, player);
                break;
            case "s_372":
                setupResponses(SCRIPT, "s_374", new String[] { "s_376", "s_378"}, player);
                break;
            case "s_300":
                setupResponses(SCRIPT, "s_302", new String[] { "s_304", "s_306"}, player);
                break;
            case "s_413":
                setupResponses(SCRIPT, "s_415", new String[] { "s_416"}, player);
                break;
            case "s_399":
                setupResponses(SCRIPT, "s_404", new String[] { "s_406", "s_407"}, player);
                break;
            case "s_140":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_142"));
                break;
            case "s_406":
                setupResponses(SCRIPT, "s_408", new String[] { "s_410", "s_409"}, player);
                break;
            case "s_108":
                setupResponses(SCRIPT, "s_110", new String[] { "s_111", "s_112"}, player);
                break;
            case "s_124":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_126"));
                break;
            case "s_430":
                setupResponses(SCRIPT, "s_432", new String[] { "s_433"}, player);
                break;
            case "s_294":
                setupResponses(SCRIPT, "s_296", new String[] { "s_298", "s_300", "s_308"}, player);
                break;
            case "s_390":
                setupResponses(SCRIPT, "s_392", new String[] { "s_394"}, player);
                break;
            case "s_239":
                setupResponses(SCRIPT, "s_241", new String[] { "s_243", "s_252", "s_244"}, player);
                break;
            case "s_116":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_119"));
                break;
            case "s_254":
            case "s_252":
            case "s_240":
                setupResponses(SCRIPT, "s_242", new String[] { "s_245", "s_246"}, player);
                break;
            case "s_245":
                setupResponses(SCRIPT, "s_247", new String[] { "s_248", "s_249"}, player);
                break;
            case "s_183":
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_185"));
                break;
            case "s_114":
                setupResponses(SCRIPT, "s_115", new String[] { "s_116", "s_117"}, player);
                break;
            case "s_132":
                setupResponses(SCRIPT, "s_136", new String[] { "s_140", "s_162"}, player);
                break;
            case "s_131":
                setupResponses(SCRIPT, "s_133", new String[] { "s_139"}, player);
                break;
            case "s_128":
                setupResponses(SCRIPT, "s_129", new String[] { "s_131"}, player);
                break;
            case "s_91":
                setupResponses(SCRIPT, "s_93", new String[] { "s_128", "s_102"}, player);
                break;
            case "s_230":
                setupResponses(SCRIPT, "s_232", new String[] { "s_235", "s_236"}, player);
                break;
            case "s_228":
                setupResponses(SCRIPT, "s_229", new String[] { "s_230", "s_231", "s_233"}, player);
                break;
            case "s_237":
            case "s_217":
                setupResponses(SCRIPT, "s_219", new String[] { "s_228", "s_227" }, player);
                break;
            case "s_216":
                setupResponses(SCRIPT, "s_218", new String[] { "s_220", "s_221" }, player);
                break;
            case "s_121":
                setupResponses(SCRIPT, "s_122", new String[] { "s_124", "s_125" }, player);
                break;
            case "s_248":
                setupResponses(SCRIPT, "s_250", "s_251", player);
                break;
            case "s_223":
                setupResponses(SCRIPT, "s_225", "s_237", player);
                break;
            case "s_220":
                setupResponses(SCRIPT, "s_222", new String[] { "s_223", "s_224" }, player);
                break;
            case "s_111":
                setupResponses(SCRIPT, "s_113", new String[] { "s_114", "s_118" }, player);
                break;
            case "s_87":
                setupResponses(SCRIPT, "s_89", new String[] { "s_91", "s_104" }, player);
                break;
        }
    }
}
