package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class aurilia_quharek extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/aurilia_quharek";

    public void OnStartNpcConversation(obj_id self, obj_id player) {
        if (groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_01")) {
            OnStartNpcConversation(SCRIPT, "s_4", "s_6", player, self);
        } else {
            chat.chat(self, player, new string_id(SCRIPT, "s_4"));
        }
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_6":
                doAnimationAction(self, "laugh");
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_8"));
                break;
        }
    }
}
