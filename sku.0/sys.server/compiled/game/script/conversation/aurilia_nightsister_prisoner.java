package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class aurilia_nightsister_prisoner extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/aurilia_nightsister_prisoner";

    public void OnStartNpcConversation(obj_id self, obj_id player) {
        doAnimationAction(self, "scare");
        if (groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_01")) {
            OnStartNpcConversation(SCRIPT, "s_6", "s_7", player, self);
        } else {
            chat.chat(self, player, new string_id(SCRIPT, "s_6"));
        }
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch(response.getAsciiId()) {
            case "s_7":
                doAnimationAction(self, "scared");
                groundquests.sendSignal(player, "axkva_min_intro_01");
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_9"));
                break;
        }
    }
}
