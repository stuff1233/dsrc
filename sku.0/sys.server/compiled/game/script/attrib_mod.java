/**
 Title:        attrib_mod
 Description:  Wrapper for an attribute modification.
 */

package script;

import java.io.Serializable;

public final class attrib_mod implements Comparable, Serializable
{
	private final static long serialVersionUID = 1253041308943833323L;

	// flags values
	private final static int AMF_changeMax     = 0x00000001;      // the mod affects the max attrib value
	private final static int AMF_attackCurrent = 0x00000002;      // the mod attack/sustain affects the current attrib value
	private final static int AMF_decayCurrent  = 0x00000004;      // the mod decay affects the current attrib value
	private final static int AMF_triggerOnDone = 0x00000008;      // cause a trigger to go off when the mod ends
	private final static int AMF_visible       = 0x00000010;      // the attrib mod info should be sent to the player
	private final static int AMF_skillMod      = 0x00000020;      // this mod affects skill mods, not attribs
	private final static int AMF_directDamage  = 0x00000040;      // the mod is direct damage that should be applied to the creature

	private String name;      // mod name (optional)
	private String skill;		// which skill the modifier affects (if this is a skill mod)
	private int attrib;		// which attribute the modifier affects (if this is an attrib mod)
	private int value;		// the modifier value
	private float duration;	// how long it lasts
	private float attack;		// time it takes to go from 0 to value
	private float decay;		// time it takes to go from value to 0
	private int flags;		// flags the behavior of the mod


	/**
	 * Class constructor. This is for use when creating an attrib_mod from C.
	 */
	private attrib_mod()
	{
	}

	/**
	 * Class constructor. Makes an old-style attrib mod.
	 *
	 * @param attrib		modifier attribute
	 * @param value			modifier value
	 * @param duration		how long it lasts
	 * @param attack		time it takes to go from 0 to value
	 * @param decay			time it takes to go from value to 0 (MOD_WOUND = wound, MOD_FAUCET = use attrib faucet)
	 */
	public attrib_mod(int attrib, int value, float duration, float attack, float decay)
	{
		name = null;
		skill = null;
		this.attrib = attrib;
		this.value = value;
		this.duration = duration;
		this.attack = attack;
		this.decay = decay;
		flags = AMF_changeMax | AMF_attackCurrent | AMF_decayCurrent;
		if (attack == 0 && duration == 0 && decay == base_class.MOD_POOL)
		{
			flags |= AMF_directDamage;
		}
	}	// attrib_mod

	/**
	 * Class constructor. Makes a new-style attrib mod.
	 *
	 * @param name          	the mod name
	 * @param attrib			modifier attribute
	 * @param value				modifier value
	 * @param duration			how long it lasts
	 * @param attack			time it takes to go from 0 to value
	 * @param decay				time it takes to go from value to 0 (MOD_WOUND = wound, MOD_FAUCET = use attrib faucet)
	 * @param changeCurrent		flag to update the current value before the decay time is reached
	 * @param triggerOnDone		flag to trigger OnAttribModDone when the mod is removed from the creature
	 * @param visible			flag to send a message to the player giving info about the mod
	 */
	public attrib_mod(String name, int attrib, int value, float duration, float attack, float decay, boolean changeCurrent, boolean triggerOnDone, boolean visible)
	{
		this.name = name;
		skill = null;
		this.attrib = attrib;
		this.value = value;
		this.duration = duration;
		this.attack = attack;
		this.decay = decay;
		flags = AMF_changeMax;
		if (changeCurrent)
			flags |= AMF_attackCurrent;
		if (triggerOnDone)
			flags |= AMF_triggerOnDone;
		if (visible)
			flags |= AMF_visible;
	}	// attrib_mod

	/**
	 * Class constructor. Makes a new-style attrib mod, as a damage/heal over time mod.
	 *
	 * @param attrib			modifier attribute
	 * @param value				modifier value
	 * @param duration			how long it lasts
	 */
	public attrib_mod(int attrib, int value, float duration)
	{
		name = null;
		skill = null;
		this.attrib = attrib;
		this.value = value;
		this.duration = 0;
		attack = duration;
		decay = base_class.MOD_POOL;
		flags = AMF_attackCurrent;
	}	// attrib_mod

	/**
	 * Class constructor. Makes a new-style attrib mod, as a skillmod mod.
	 *
	 * @param name          	the mod name
	 * @param skill				the skill to effect
	 * @param value				modifier value
	 * @param duration			how long it lasts
	 * @param triggerOnDone		flag to trigger OnSkillModDone when the mod is removed from the creature
	 * @param visible			flag to send a message to the player giving info about the mod
	 */
	public attrib_mod(String name, String skill, int value, float duration, boolean triggerOnDone, boolean visible)
	{
		this.name = name;
		this.skill = skill;
		this.value = value;
		this.duration = duration;
		attack = 0;
		decay = 0;
		flags = AMF_skillMod;
		if (triggerOnDone)
			flags |= AMF_triggerOnDone;
		if (visible)
			flags |= AMF_visible;
	}	// attrib_mod

	/**
	 * Class constructor. Unpacks string data created by the pack() function.
	 *
	 * @param packedData		string returned from the pack() function
	 */
	public attrib_mod(String packedData)
	{
		String[] values = base_class.split(packedData, ' ');
		if (values == null || (values.length != 8 && values.length != 5))
		{
			System.err.println("ERROR in Java attrib_mod constructor, invalid data " + packedData);
			attrib = -1;
		}
		else if (values.length == 8)
		{
			// new style mod
			name = values[0];
			if (name.equals("?"))
				name = null;
			skill = values[1];
			if (skill.equals("?"))
				skill = null;
			attrib = Integer.parseInt(values[2]);
			value = Integer.parseInt(values[3]);
			duration = Float.parseFloat(values[4]);
			attack = Float.parseFloat(values[5]);
			decay = Float.parseFloat(values[6]);
			flags = Integer.parseInt(values[7]);
		}
		else
		{
			// old style mod
			name = null;
			skill = null;
			attrib = Integer.parseInt(values[0]);
			value = Integer.parseInt(values[1]);
			duration = Float.parseFloat(values[2]);
			attack = Float.parseFloat(values[3]);
			decay = Float.parseFloat(values[4]);
			flags = AMF_changeMax | AMF_attackCurrent | AMF_decayCurrent;
		}
	}	// attrib_mod

	 /**
	  * Copy constructor.
	  *
	  * @param src		class instance to copy
	  */
	public attrib_mod(attrib_mod src)
	{
		name = src.name;
		skill = src.skill;
		attrib = src.attrib;
		value = src.value;
		duration = src.duration;
		attack = src.attack;
		decay = src.decay;
		flags = src.flags;
	}	// attrib_mod(attrib_mod)

	/**
	 * Tests if this mod is an attrib mod.
	 *
	 * @return true if we are an attrib mod, false if not
	 */
	public boolean isAttribMod()
	{
		return (flags & AMF_changeMax) != 0;
	}	// isAttribMod

	/**
	 * Tests if this mod is a skillmod mod.
	 *
	 * @return true if we are an skillmod mod, false if not
	 */
	public boolean isSkillModMod()
	{
		return (flags & AMF_skillMod) != 0;
	}	// isSkillModMod

	/**
	 * Tests if this mod is a dot/hot mod.
	 *
	 * @return true if we are a dot mod, false if not
	 */
	public boolean isDotMod()
	{
		return !isAttribMod() && !isSkillModMod();
	}	// isDotMod

	/**
	 * Accessor function.
	 *
	 * @return the modifier name
	 */
	public String getName()
	{
		return name;
	}	// getName

	/**
	 * Accessor function.
	 *
	 * @return the modifier skillmod name
	 */
	public String getSkillModName()
	{
		return skill;
	}	// getSkillModName

	/**
	 * Accessor function.
	 *
	 * @return the modifier attribute
	 */
	public int getAttribute()
	{
		return attrib;
	}	// getAttribute

	/**
	 * Accessor function.
	 *
	 * @return the modifier value
	 */
	public int getValue()
	{
		return value;
	}	// getValue

	/**
	 * Accessor function.
	 *
	 * @return the modifier duration
	 */
	public float getDuration()
	{
		return duration;
	}	// getDuration

	/**
	 * Accessor function.
	 *
	 * @return the modifier attack
	 */
	public float getAttack()
	{
		return attack;
	}	// getAttack

	/**
	 * Accessor function.
	 *
	 * @return the modifier decay
	 */
	public float getDecay()
	{
		return decay;
	}	// getDecay

	/**
	 * Packs the attrib mod into a string for storing as an objvar.
	 *
	 * @return the packed data
	 */
	public String pack()
	{
		if (name == null || name.length() == 0)
			name = "";
		if (skill == null || skill.length() == 0)
			skill = "";
		return name + " " +
			skill + " " +
			Integer.toString(attrib) + " " +
			Integer.toString(value) + " " +
			Float.toString(duration) + " " +
			Float.toString(attack) + " " +
			Float.toString(decay) + " " +
			Integer.toString(flags);
	}	// pack

	/**
	 * Conversion function.
	 *
	 * @return the modifier as a string.
	 */
	public String toString()
	{
		if (name == null)
			name = "";
		if (skill == null)
			skill = "";
		return "(name=" + name +
			", skill=" + skill +
			", attrib=" + Integer.toString(attrib) +
			", value=" + Integer.toString(value) +
			", duration=" + Float.toString(duration) +
			", attack=" + Float.toString(attack) +
			", decay=" + Float.toString(decay) +
			", flags=" + Integer.toString(flags) +
			")";
	}	// toString

	/**
	 * Compares this to a generic object.
	 *
	 * @returns <, =, or > 0 if the object is a attribMod, else throws
	 *		ClassCastException
	 */
	public int compareTo(Object o) throws ClassCastException
	{
		return compareTo((attrib_mod)o);
	}	// compareTo(Object)

	/**
	 * Compares this to another attribMod.
	 *
	 * @returns <, =, or > 0
	 */
	public int compareTo(attrib_mod id)
	{
		if (attrib == id.attrib)
		{
			if (value == id.value)
			{
				if (duration == id.duration)
				{
					if (attack == id.attack)
					{
						if (decay == id.decay)
							return 0;
						return decay < id.decay ? -1 : 1;
					}
					return attack < id.attack ? -1 : 1;
				}
				return duration < id.duration ? -1 : 1;
			}
			return value - id.value;
		}
		return attrib - id.attrib;
	}	// compareTo(attrib_mod)

	/**
	 * Compares this to a generic object.
	 *
	 * @returns true if the objects have the same data, false if not
	 */
	public boolean equals(Object o)
	{
		try
		{
			int result = compareTo(o);
			if (result == 0)
				return true;
		}
		catch (ClassCastException err)
		{
		}
		return false;
	}	// equals

}	// class objId
