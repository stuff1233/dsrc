package script.structure.municipal;

import script.*;

import script.library.city;
import script.library.travel;

public class starport_city extends script.structure.municipal.starport
{
    public starport_city()
    {
    }
    public int OnAttach(obj_id self)
    {
        setupStarport(self, null);
        return super.OnAttach(self);
    }
    public int OnInitialize(obj_id self)
    {
        String travel_point = travel.getTravelPointName(self);
        int travel_cost = travel.getTravelCost(self);
        if (travel_point == null || travel_cost == -1)
        {
            return super.OnInitialize(self);
        }
        travel.initializeStarport(self, travel_point, travel_cost, true);
        return super.OnInitialize(self);
    }
    public int setupStarport(obj_id self, dictionary params)
    {
        int city_id = getCityAtLocation(getLocation(self), 0);
        int cost = cityGetTravelCost(city_id);
        if (cost > 0)
        {
            destroyObject(self);
            return SCRIPT_CONTINUE;
        }
        String cityName = cityGetName(city_id);
        String travel_point = cityName;
        int travel_cost = 100;
        travel.initializeStarport(self, travel_point, travel_cost, true);
        return SCRIPT_CONTINUE;
    }
    public int OnDestroy(obj_id self)
    {
        boolean initd = false;
        obj_id[] objects = getObjIdArrayObjVar(self, "travel.base_object");
        for (int i = 0; i < objects.length; i++)
        {
            destroyObject(objects[i]);
            initd = true;
        }
        if (initd)
        {
            city.removeStarport(self);
        }
        return super.OnInitialize(self);
    }
}
