package script.space.terminal;

import script.*;
import java.util.ArrayList;
import java.util.List;

import script.library.ai_lib;
import script.library.callable;
import script.library.performance;
import script.library.player_structure;
import script.library.regions;
import script.library.session;
import script.library.space_crafting;
import script.library.space_transition;
import script.library.space_utils;
import script.library.stealth;
import script.library.sui;
import script.library.travel;
import script.library.utils;
import script.library.vehicle;
import script.library.buff;
import script.library.event_perk;

public class terminal_space extends script.terminal.base.base_terminal {
    public static final float TERMINAL_USE_DISTANCE = 8.0f;
    public static final string_id SID_LAUNCH_SHIP = new string_id("space/space_terminal", "launch_ship");
    public static final string_id SID_NOT_IN_COMBAT = new string_id("travel", "not_in_combat");
    public static final String LAUNCH_LOCATIONS = "datatables/space_zones/launch_locations.iff";

    public int OnInitialize(obj_id self) {
        requestPreloadCompleteTrigger(self);
        return SCRIPT_CONTINUE;
    }

    public int OnPreloadComplete(obj_id self) {
        String strName = "mos_eisley";
        dictionary dctTeleportInfo = null;
        location locTest = getLocation(self);
        region[] rgnCities = getRegionsWithGeographicalAtPoint(locTest, regions.GEO_CITY);
        if (rgnCities != null && rgnCities.length > 0) {
            for (int i = 0; i < rgnCities.length; i++) {
                region rgnTest = rgnCities[i];
                strName = rgnTest.getName();
                if (strName.startsWith("@")) {
                    string_id strTest = utils.unpackString(strName);
                    strName = strTest.getAsciiId();
                    dctTeleportInfo = dataTableGetRow(LAUNCH_LOCATIONS, strName);
                    if (dctTeleportInfo != null) {
                        break;
                    }
                }
            }
        }
        if (dctTeleportInfo == null) {
            dctTeleportInfo = dataTableGetRow(LAUNCH_LOCATIONS, strName);
        }
        utils.setScriptVar(self, "space.loc.space", new location(dctTeleportInfo.getFloat("spaceX"), dctTeleportInfo.getFloat("spaceY"), dctTeleportInfo.getFloat("spaceZ"), dctTeleportInfo.getString("spaceScene")));
        utils.setScriptVar(self, "space.locationName", "w" + dctTeleportInfo.getInt("spaceLocationIndex"));
        utils.setScriptVar(self, "space.loc.ground", new location(dctTeleportInfo.getFloat("groundX"), dctTeleportInfo.getFloat("groundY"), dctTeleportInfo.getFloat("groundZ"), dctTeleportInfo.getString("groundScene")));
        utils.setScriptVar(self, "ground.locationName", dctTeleportInfo.getString("pointName"));
        return SCRIPT_CONTINUE;
    }

    public int OnAboutToLaunchIntoSpace(obj_id self, obj_id player, obj_id shipControlDevice, obj_id[] membersApprovedByShipOwner, String destinationGroundPlanet, String destinationGroundTravelPoint) {
        if (!doSpacePrecheck(player)) {
            return SCRIPT_CONTINUE;
        }
        if (ai_lib.isInCombat(player)) {
            sendSystemMessage(player, SID_NOT_IN_COMBAT);
            return SCRIPT_CONTINUE;
        }
        boolean isStarportToStarportLaunch = destinationGroundPlanet != null && !destinationGroundPlanet.equals("");
        if (travel.isTravelBlocked(player, !isStarportToStarportLaunch)) {
            return SCRIPT_CONTINUE;
        }
        if (getState(self, STATE_GLOWING_JEDI) != 0) {
            setState(self, STATE_GLOWING_JEDI, false);
        }
        location warpLocation = utils.getLocationScriptVar(self, "space.loc.space");
        if (warpLocation != null) {
            if (isIdValid(shipControlDevice)) {
                obj_id ship = space_transition.getShipFromShipControlDevice(shipControlDevice);
                boolean success = space_crafting.checkForCollectionReactor(shipControlDevice, ship);
                if (success) {
                    CustomerServiceLog("ShipComponents", "Collection reactor found and handled for: (" + self + ") " + getName(self));
                    return SCRIPT_CONTINUE;
                }
                space_crafting.setCollectionReactorChecked(shipControlDevice);
                if (!space_utils.isShipUsable(ship, player)) {
                    return SCRIPT_CONTINUE;
                }
                float currentShipMass = getChassisComponentMassCurrent(ship);
                float maxAllowableShipMass = getChassisComponentMassMaximum(ship);
                if ((currentShipMass > maxAllowableShipMass) && !isGod(player)) {
                    string_id strSpam = new string_id("space/space_interaction", "too_heavy");
                    sendSystemMessage(player, strSpam);
                    return SCRIPT_CONTINUE;
                }
                if (space_utils.isShipWithInterior(ship)) {
                    obj_id[] objControlDevices = space_transition.findShipControlDevicesForPlayer(player, true);
                    int intCount = 0;
                    for (int i = 0; i < objControlDevices.length; i++) {
                        obj_id objTestShip = space_transition.getShipFromShipControlDevice(objControlDevices[i]);
                        if (space_utils.isShipWithInterior(objTestShip)) {
                            if (objTestShip != ship) {
                                int intItemCount = player_structure.getStructureNumItems(objTestShip);
                                if (intItemCount > 0) {
                                    intCount = intCount + 1;
                                    if (intCount >= 2) {
                                        string_id strSpam = new string_id("space/space_interaction", "too_many_pobs");
                                        sendSystemMessage(player, strSpam);
                                        return SCRIPT_CONTINUE;
                                    }
                                }
                            }
                        }
                    }
                }
                if (isIdValid(ship)) {
                    utils.setLocalVar(player, "objControlDevice", shipControlDevice);
                    callable.storeCallables(player);
                    vehicle.storeAllVehicles(player);
                    if (utils.hasScriptVar(player, "currentHolo")) {
                        performance.holographicCleanup(player);
                    }
                    if (isStarportToStarportLaunch) {
                        doStarportToStarportLaunch(player, ship, membersApprovedByShipOwner, destinationGroundPlanet, destinationGroundTravelPoint);
                    } else {
                        location locDestination = space_utils.getRandomLocationInSphere(warpLocation, 150, 300);
                        location locFinalDestination = getFinalHyperspaceLocation(player, locDestination);
                        if (locFinalDestination == null) {
                            string_id tooFull = new string_id("shared_hyperspace", "zone_too_full_use_travel");
                            sendSystemMessage(player, tooFull);
                            return SCRIPT_CONTINUE;
                        }
                        utils.setScriptVar(player, "strLaunchPointName", utils.getStringScriptVar(self, "space.locationName"));
                        location groundLoc = utils.getLocationScriptVar(self, "space.loc.ground");
                        session.logActivity(player, session.ACTIVITY_SPACE_LAUNCH);
                        if (hasScript(player, performance.DANCE_HEARTBEAT_SCRIPT)) {
                            performance.stopDance(player);
                        }
                        if (hasScript(player, performance.MUSIC_HEARTBEAT_SCRIPT)) {
                            performance.stopMusic(player);
                        }
                        if (membersApprovedByShipOwner != null && membersApprovedByShipOwner.length > 0) {
                            for (int i = 0; i < membersApprovedByShipOwner.length; ++i) {
                                if (hasScript(membersApprovedByShipOwner[i], performance.DANCE_HEARTBEAT_SCRIPT)) {
                                    performance.stopDance(membersApprovedByShipOwner[i]);
                                }
                                if (hasScript(membersApprovedByShipOwner[i], performance.MUSIC_HEARTBEAT_SCRIPT)) {
                                    performance.stopMusic(membersApprovedByShipOwner[i]);
                                }
                            }
                        }
                        launch(player, ship, membersApprovedByShipOwner, locFinalDestination, groundLoc);
                    }
                    return SCRIPT_CONTINUE;
                }
            }
        }
        return SCRIPT_CONTINUE;
    }

    public void doStarportToStarportLaunch(obj_id player, obj_id ship, obj_id[] membersApprovedByShipOwner, String planet, String pointName) {
        if (!getPlanetTravelPointInterplanetary(planet, pointName)) {
            return;
        }
        if (space_utils.isBasicShip(ship)) {
            if (!planet.equals(getLocation(player).getArea())) {
                string_id strSpam = new string_id("space/space_interaction", "no_travel_basic");
                sendSystemMessage(player, strSpam);
                return;
            }
        }
        if (callable.hasAnyCallable(player)) {
            callable.storeCallables(player);
        }
        List<obj_id> groupMembersToWarp = new ArrayList<>();
        groupMembersToWarp.add(player);
        List shipStartLocations = space_transition.getShipStartLocations(ship);
        if (shipStartLocations != null && shipStartLocations.size() > 0) {
            int startIndex = 0;
            if (isIdValid(getLocation(player).getCell())) {
                for (int i = 0; i < membersApprovedByShipOwner.length; ++i) {
                    if (membersApprovedByShipOwner[i] != player && exists(membersApprovedByShipOwner[i]) && getLocation(membersApprovedByShipOwner[i]).getCell() == getLocation(player).getCell()) {
                        startIndex = space_transition.getNextStartIndex(shipStartLocations, startIndex);
                        if (startIndex <= shipStartLocations.size()) {
                            groupMembersToWarp.add(membersApprovedByShipOwner[i]);
                        }
                        if (callable.hasAnyCallable(membersApprovedByShipOwner[i])) {
                            callable.storeCallables(membersApprovedByShipOwner[i]);
                        }
                    }
                }
            }
        }
        for (obj_id groupMember : groupMembersToWarp) {
            travel.movePlayerToDestination(groupMember, planet, pointName);
        }
    }

    public void launch(obj_id player, obj_id ship, obj_id[] membersApprovedByShipOwner, location warpLocation, location groundLoc) {
        if (callable.hasAnyCallable(player)) {
            callable.storeCallables(player);
        }
        stealth.checkForAndMakeVisible(player);
        int shapechange = buff.getBuffOnTargetFromGroup(player, "shapechange");
        if (shapechange != 0) {
            buff.removeBuff(player, shapechange);
            sendSystemMessage(player, event_perk.SHAPECHANGE_SPACE);
        }
        space_transition.clearOvertStatus(ship);
        List<obj_id> groupMembersToWarp = new ArrayList<>();
        groupMembersToWarp.add(player);
        List<Integer> groupMemberStartIndex = new ArrayList<>();
        groupMemberStartIndex.add(0);
        utils.setScriptVar(player, "strLaunchPointName", "launching");
        List shipStartLocations = space_transition.getShipStartLocations(ship);
        if (shipStartLocations != null && shipStartLocations.size() > 0) {
            int startIndex = 0;
            location playerLoc = getLocation(player);
            if (isIdValid(playerLoc.getCell())) {
                for (int i = 0; i < membersApprovedByShipOwner.length; ++i) {
                    if (membersApprovedByShipOwner[i] != player && exists(membersApprovedByShipOwner[i]) && getLocation(membersApprovedByShipOwner[i]).getCell() == playerLoc.getCell()) {
                        startIndex = space_transition.getNextStartIndex(shipStartLocations, startIndex);
                        if (startIndex <= shipStartLocations.size()) {
                            groupMembersToWarp.add(membersApprovedByShipOwner[i]);
                            groupMemberStartIndex.add(startIndex);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < groupMembersToWarp.size(); ++i) {
            if (callable.hasAnyCallable(groupMembersToWarp.get(i))) {
                callable.storeCallables(groupMembersToWarp.get(i));
            }
            stealth.checkForAndMakeVisible(groupMembersToWarp.get(i));
            shapechange = buff.getBuffOnTargetFromGroup(groupMembersToWarp.get(i), "shapechange");
            if (shapechange != 0) {
                buff.removeBuff(groupMembersToWarp.get(i), shapechange);
                sendSystemMessage(groupMembersToWarp.get(i), event_perk.SHAPECHANGE_SPACE);
            }
            space_transition.setLaunchInfo(groupMembersToWarp.get(i), ship, groupMemberStartIndex.get(i), groundLoc);
            warpPlayer(groupMembersToWarp.get(i), warpLocation.getArea(), warpLocation.getX(), warpLocation.getY(), warpLocation.getZ(), null, warpLocation.getX(), warpLocation.getY(), warpLocation.getZ());
        }
    }

    public boolean doSpacePrecheck(obj_id objPlayer) {
        if (isIncapacitated(objPlayer)) {
            string_id strSpam = new string_id("space/space_interaction", "no_use_terminal_incapacitated");
            sendSystemMessage(objPlayer, strSpam);
            return false;
        } else if (isDead(objPlayer)) {
            string_id strSpam = new string_id("space/space_interaction", "no_use_terminal_dead");
            sendSystemMessage(objPlayer, strSpam);
            return false;
        } else if (getState(objPlayer, STATE_COMBAT) > 0) {
            string_id strSpam = new string_id("space/space_interaction", "no_use_terminal_combat");
            sendSystemMessage(objPlayer, strSpam);
            return false;
        }
        return true;
    }

    public void doHyperspaceSelector(obj_id self, obj_id objPlayer) {
        String strFileName = "datatables/space/hyperspace/hyperspace_locations.iff";
        String[] strPointNames = dataTableGetStringColumnNoDefaults(strFileName, "HYPERSPACE_POINT_NAME");
        utils.setScriptVar(self, "strPointNames", strPointNames);
        String[] strLocalizedNames = new String[strPointNames.length];
        for (int i = 0; i < strPointNames.length; i++) {
            strLocalizedNames[i] = utils.packStringId(new string_id("hyperspace_points_n", strPointNames[i]));
        }
        String strPrompt = "Choose a Hyperspace point";
        sui.listbox(self, objPlayer, strPrompt, strLocalizedNames, "chooseHyperspaceSpot");
    }

    public int chooseHyperspaceSpot(obj_id self, dictionary params) {
        if (params == null) {
            return SCRIPT_CONTINUE;
        }
        obj_id player = sui.getPlayerId(params);
        if (!isIdValid(player)) {
            return SCRIPT_CONTINUE;
        } else if (sui.getIntButtonPressed(params) == sui.BP_CANCEL) {
            return SCRIPT_CONTINUE;
        }
        int intIndex = sui.getListboxSelectedRow(params);
        if (intIndex == -1) {
            return SCRIPT_CONTINUE;
        }
        String[] strPointNames = utils.getStringArrayScriptVar(self, "strPointNames");
        if (intIndex > strPointNames.length - 1) {
            return SCRIPT_CONTINUE;
        }
        String strFileName = "datatables/space/hyperspace/hyperspace_locations.iff";
        dictionary dctPointInfo = dataTableGetRow(strFileName, strPointNames[intIndex]);
        if (dctPointInfo == null) {
            return SCRIPT_CONTINUE;
        }
        location warpLocation = new location();
        warpLocation.setX((float)dctPointInfo.getInt("X"));
        warpLocation.setY((float)dctPointInfo.getInt("Y"));
        warpLocation.setZ((float)dctPointInfo.getInt("Z"));
        warpLocation.setArea(dctPointInfo.getString("SCENE"));
        obj_id shipControlDevice = utils.getObjIdLocalVar(player, "objControlDevice");
        obj_id ship = space_transition.getShipFromShipControlDevice(shipControlDevice);
        if (!space_utils.isShipUsable(ship, player)) {
            return SCRIPT_CONTINUE;
        } else if (isIdValid(ship)) {
            obj_id[] membersApprovedByShipOwner = new obj_id[0];
            location locDestination = space_utils.getRandomLocationInSphere(warpLocation, 150, 300);
            location groundLoc = utils.getLocationScriptVar(self, "space.loc.ground");
            launch(player, ship, membersApprovedByShipOwner, locDestination, groundLoc);
            return SCRIPT_CONTINUE;
        }
        sendDirtyObjectMenuNotification(self);
        return SCRIPT_CONTINUE;
    }

    public location getFinalHyperspaceLocation(obj_id objPlayer, location locTest) {
        return locTest;
    }

    public location checkZonePopulation(location[] locTest) {
        for (int i = 0; i < locTest.length; i++) {
            if (!isAreaTooFullForTravel(locTest[i].getArea(), 0, 0)) {
                return locTest[i];
            }
        }
        return null;
    }
}
