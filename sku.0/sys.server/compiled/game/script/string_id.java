/*
 Title:        string_id
 Description:  Wrapper for a string table id.
 */

package script;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class string_id implements Comparable, Serializable {
	private final static long serialVersionUID = -1331982663286942264L;

	// A string_id is represented by a table name, and either an ascii text id
	// or an integer index id. The ascii text id is the default id to use; the
	// index id will only be used if the ascii id is 0 length or null
	protected           String table   = "";		// table/file of string id
	protected transient String asciiId = "";	// english ascii text for string id
	protected transient int indexId;

	public string_id(String x, int i) {
	}

	public string_id(String table, String id) {
		if (table != null)
			this.table = table.toLowerCase();
		if (id != null)
			asciiId = id.toLowerCase();
	}

	public string_id(string_id src) {
		table = src.table;
		asciiId = src.asciiId;
	}

	public String getTable() {
		return table;
	}

	public String getAsciiId() {
		return asciiId;
	}

	public boolean isEmpty () {
		return table.isEmpty() && asciiId.isEmpty();
	}

	public String toString() {
		StringBuffer sbuf = new StringBuffer(table);
		sbuf.append (':');
		sbuf.append (asciiId);
		return sbuf.toString ();
	}

	public int compareTo(Object o) throws ClassCastException {
		return compareTo((string_id)o);
	}

	public int compareTo(string_id id) {
		int result = table.compareTo(id.table);
		if ( result == 0 ) {
			result = asciiId.compareTo(id.asciiId);
		}
		return result;
	}

	public boolean equals(String o) {
		return asciiId.equals(o);
	}

	public boolean equals(string_id o) {
		return table.equals(o.table) && asciiId.equals(o.asciiId);
	}

	public prose_package getPackage() {
		return new prose_package(table, asciiId);
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		if (asciiId.length() > 0) {
			
			out.writeBoolean(true);
			out.writeObject(asciiId);
		} else {
			out.writeBoolean(false);
			out.writeInt(indexId);
		}
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		if (in.readBoolean()) {
			asciiId = (String)in.readObject();
		} else {
			indexId = in.readInt();
		}
	}
}
