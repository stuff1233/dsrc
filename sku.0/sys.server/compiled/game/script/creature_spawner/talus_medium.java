package script.creature_spawner;

import script.*;

public class talus_medium extends script.creature_spawner.base_newbie_creature_spawner
{
    public talus_medium()
    {
    }
    public int OnInitialize(obj_id self)
    {
        return SCRIPT_CONTINUE;
    }
    public int creatureDied(obj_id self, dictionary params)
    {
        return SCRIPT_CONTINUE;
    }
    public String pickCreature()
    {
        return null;
    }
    public int getMaxPop()
    {
        return 0;
    }
}
