package script.creature_spawner;

import script.dictionary;
import script.obj_id;

public class death_msg extends script.base_script
{
    public int OnDestroy(obj_id self)
    {
        dictionary webster = new dictionary();
        webster.put("deadGuy", self);
        messageTo(getObjIdObjVar(self, "creater"), "creatureDied", webster, rand(180, 360), false);
        return SCRIPT_CONTINUE;
    }
}
