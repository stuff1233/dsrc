package script.creature_spawner;

import script.dictionary;
import script.obj_id;

public class rori_npc_easy extends base_newbie_npc_spawner
{
    public byte maxPop = 4;

    public String pickCreature()
    {
        switch (rand(1, 4))
        {
            case 1:
                return "garyn_prowler";
            case 2:
                return "gundark_crook";
            case 3:
                return "cobral_mugger";
            default:
                return "gundark_hooligan";
        }
    }
    public int creatureDied(obj_id self, dictionary params)
    {
        if (params == null || params.isEmpty())
        {
            LOG("sissynoid", "Spawner " + self + " on Rori. Rori_npc_easy script had Invalid Params from the deadGuy.");
            CustomerServiceLog("SPAWNER_OVERLOAD", "Spawner " + self + " on Rori. Rori_npc_easy script had Invalid Params from the deadGuy.");
            return SCRIPT_CONTINUE;
        }
        return super.creatureDied(self, params);
    }
}
