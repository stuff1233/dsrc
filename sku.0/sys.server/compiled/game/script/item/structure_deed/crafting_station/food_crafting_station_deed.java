package script.item.structure_deed.crafting_station;

import script.*;

public class food_crafting_station_deed extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        setObjVar(self, "unUsed", 1);
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi)
    {
        menu_info_data mid = mi.getMenuItemByType(menu_info_types.ITEM_USE);
        if (mid != null)
        {
            mid.setServerNotify(true);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item)
    {
        if (item == menu_info_types.ITEM_USE)
        {
            if (!hasObjVar(self, "usedUp"))
            {
                location locTest = new location(getLocation(player));
                locTest.setX(locTest.getX() + 5);
                locTest.setZ(locTest.getZ() + 5);
                obj_id harvesterObject = createObject("object/tangible/crafting/station/food_station.iff", locTest);
                setObjVar(self, "usedUp", 1);
                if (harvesterObject == null)
                {
                    return SCRIPT_OVERRIDE;
                }
                else 
                {
                    destroyObject(self);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
}
