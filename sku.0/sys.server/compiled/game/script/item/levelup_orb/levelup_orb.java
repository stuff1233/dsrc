package script.item.levelup_orb;

import script.*;
import script.base_script;

import script.library.utils;
import script.library.xp;

public class levelup_orb extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        if (!hasScript(self, "item.special.nomove"))
            attachScript(self, "item.special.nomove");
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi)
    {
        if (canManipulate(player, self, true, true, 15, true))
        {
            if (utils.isNestedWithinAPlayer(self))
            {
                menu_info_data mid = mi.getMenuItemByType(menu_info_types.ITEM_USE);
                if (mid != null)
                    mid.setServerNotify(true);
                else 
                    mi.addRootMenu(menu_info_types.ITEM_USE, new string_id("ui_radial", "item_use"));
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item)
    {
        if (utils.getContainingPlayer(self) != player)
            return SCRIPT_CONTINUE;

        if (item == menu_info_types.ITEM_USE)
        {
            if (canManipulate(player, self, true, true, 15, true))
            {
                int combatLevel = getLevel(player);
                if (combatLevel == 1) {
                    sendSystemMessage(player, new string_id("event_perk", "levelup_orb_badclass"));
                } else if (combatLevel == 90) {
                    sendSystemMessage(player, new string_id("event_perk", "levelup_orb_toohigh"));
                } else {
                    raiseLevel(player);
                    destroyObject(self);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
    public void raiseLevel(obj_id player)
    {
        int currentLevel = getLevel(player);
        int xpNeeded = dataTableGetInt("datatables/player/player_level.iff", currentLevel, "xp_required");
        int prevNeeded = dataTableGetInt("datatables/player/player_level.iff", currentLevel - 1, "xp_required");
        xpNeeded -= prevNeeded;
        xp.grant(player, "combat_general", xpNeeded);
    }
}
