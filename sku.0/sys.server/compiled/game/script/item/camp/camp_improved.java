package script.item.camp;

import script.*;

public class camp_improved extends script.item.camp.camp_base
{
    public int OnAttach(obj_id self)
    {
        setObjVar(self, "campPower", 3);
        setObjVar(self, "skillReq", 30);
        return SCRIPT_CONTINUE;
    }
}
