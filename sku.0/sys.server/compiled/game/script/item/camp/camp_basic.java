package script.item.camp;

import script.*;

public class camp_basic extends script.item.camp.camp_base
{
    public int OnAttach(obj_id self)
    {
        setObjVar(self, "campPower", 1);
        setObjVar(self, "skillReq", 1);
        return SCRIPT_CONTINUE;
    }
}
