package script.item.camp;

import script.*;

public class camp_multi extends script.item.camp.camp_base
{
    public int OnAttach(obj_id self)
    {
        setObjVar(self, "campPower", 2);
        setObjVar(self, "skillReq", 20);
        return SCRIPT_CONTINUE;
    }
}
