package script.item.wearable;

import script.*;

public class set_non_armor_wearable_socket extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        setSocket(self, null);
        return SCRIPT_CONTINUE;
    }
    public int setSocket(obj_id self, dictionary params)
    {
        int socketCount = 0;
        if (hasObjVar(self, "socket_count"))
        {
            socketCount = getIntObjVar(self, "socket_count");
        }
        if (socketCount > 0)
        {
            setSkillModSockets(self, socketCount);
        }
        if (getSkillModSockets(self) > 0)
        {
            detachScript(self, "item.wearable.set_non_armor_wearable_socket");
        }
        return SCRIPT_CONTINUE;
    }
}
