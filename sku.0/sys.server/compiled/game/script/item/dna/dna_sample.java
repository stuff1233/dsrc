package script.item.dna;

import script.*;
import script.base_class.*;
import script.combat_engine.*;

public class dna_sample extends script.base_script
{
    public int OnInitialize(obj_id self)
    {
        if (!isCrafted(self))
        {
            setCraftedId(self, self);
        }
        return SCRIPT_CONTINUE;
    }
}
