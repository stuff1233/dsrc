package script.item.special;

import script.*;

public class serialize_component extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        boolean success = true;
        success &= setCraftedId(self, self);
        success &= setCrafter(self, self);
        if (success)
        {
            detachScript(self, "item.special.serialize_component");
        }
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        boolean success = true;
        success &= setCraftedId(self, self);
        success &= setCrafter(self, self);
        if (success)
        {
            detachScript(self, "item.special.serialize_component");
        }
        return SCRIPT_CONTINUE;
    }
}
