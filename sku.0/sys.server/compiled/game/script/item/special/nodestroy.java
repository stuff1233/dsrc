package script.item.special;

import script.*;

public class nodestroy extends script.base_script
{
    public int OnDestroy(obj_id self)
    {
        obj_id owner = getOwner(self);
        if (isIdValid(owner) && isPlayer(owner) && isGod(owner))
        {
            return SCRIPT_CONTINUE;
        }
        if (isIdValid(owner) && isPlayer(owner))
        {
            prose_package pp = new prose_package("error_message", "unable_to_destroy");
            pp.setTT(self);
            sendSystemMessageProse(owner, pp);
        }
        return SCRIPT_OVERRIDE;
    }
}
