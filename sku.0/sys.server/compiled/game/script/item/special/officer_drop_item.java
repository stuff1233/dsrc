package script.item.special;

import script.*;
import script.library.utils;

public class officer_drop_item extends script.base_script
{
    public officer_drop_item()
    {
    }
    public static final float LIFESPAN = 18000.0f;
    public int OnAttach(obj_id self)
    {
        float rightNow = getGameTime();
        setObjVar(self, "item.temporary.time_stamp", rightNow);
        float dieTime = getDieTime(self);
        if (dieTime < 1)
        {
            dieTime = 1.0f;
        }
        messageTo(self, "cleanUp", null, dieTime, false);
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        float dieTime = getDieTime(self);
        if (dieTime < 1)
        {
            dieTime = 1.0f;
        }
        messageTo(self, "cleanUp", null, dieTime, false);
        return SCRIPT_CONTINUE;
    }
    public float getDieTime(obj_id tempObject)
    {
        float timeStamp = getFloatObjVar(tempObject, "item.temporary.time_stamp");
        float deathStamp = timeStamp + LIFESPAN;
        float rightNow = getGameTime();
        float dieTime = deathStamp - rightNow;
        return dieTime;
    }
    public int cleanUp(obj_id self, dictionary params)
    {
        if (self.isBeingDestroyed())
        {
            return SCRIPT_CONTINUE;
        }
        float dieTime = getDieTime(self);
        if (dieTime < 1)
        {
            destroyObject(self);
        }
        else 
        {
            messageTo(self, "cleanUp", null, dieTime, false);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnGetAttributes(obj_id self, obj_id player, String[] names, String[] attribs)
    {
        int idx = utils.getValidAttributeIndex(names);
        if (idx == -1)
        {
            return SCRIPT_CONTINUE;
        }
        int timeLeft = (int)getDieTime(self);
        names[idx] = "storyteller_time_remaining";
        attribs[idx++] = utils.formatTimeVerbose(timeLeft);
        return SCRIPT_CONTINUE;
    }
}
