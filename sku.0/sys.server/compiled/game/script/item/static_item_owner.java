package script.item;

import script.obj_id;

import script.library.utils;
import script.library.static_item;

public class static_item_owner extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        obj_id player = utils.getContainingPlayer(self);
        if (isIdValid(player))
        {
            static_item.origOwnerCheckStamp(self, player);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        obj_id player = utils.getContainingPlayer(self);
        if (isIdValid(player))
        {
            static_item.origOwnerCheckStamp(self, player);
        }
        return SCRIPT_CONTINUE;
    }
}
