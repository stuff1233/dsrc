package script.player.skill;

import script.*;

public class meditate extends script.base_script {
    public int OnAttach(obj_id self) {
        detachScript(self, "player.skill.meditate");
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self) {
        detachScript(self, "player.skill.meditate");
        return SCRIPT_CONTINUE;
    }
}
