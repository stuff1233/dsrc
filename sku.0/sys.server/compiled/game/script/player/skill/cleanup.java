package script.player.skill;

import script.*;
import script.library.meditation;
import script.library.metrics;

public class cleanup extends script.base_script {
    public int OnRecapacitated(obj_id self) {
        if (hasObjVar(self, meditation.VAR_FORCE_OF_WILL_ACTIVE)) {
            int stamp = getIntObjVar(self, meditation.VAR_FORCE_OF_WILL_ACTIVE);
            if (stamp < 0) {
                removeObjVar(self, meditation.VAR_FORCE_OF_WILL_ACTIVE);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int handlePowerBoostWane(obj_id self, dictionary params) {
        if (!hasObjVar(self, meditation.VAR_POWERBOOST_ACTIVE)) {
            return SCRIPT_CONTINUE;
        }
        int expire = getIntObjVar(self, meditation.VAR_POWERBOOST_ACTIVE);
        int d_expire = params.getInt("expiration");
        if (expire != d_expire) {
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_CONTINUE;
    }
    public int handlePowerBoostEnd(obj_id self, dictionary params) {
        if (!hasObjVar(self, meditation.VAR_POWERBOOST_ACTIVE)) {
            return SCRIPT_CONTINUE;
        }
        int expire = getIntObjVar(self, meditation.VAR_POWERBOOST_ACTIVE);
        int d_expire = params.getInt("expiration");
        if (expire != d_expire) {
            return SCRIPT_CONTINUE;
        }
        removeObjVar(self, meditation.VAR_POWERBOOST_ACTIVE);
        return SCRIPT_CONTINUE;
    }
    public int handlePowerBoostLog(obj_id self, dictionary params) {
        metrics.logBuffStatus(self);
        return SCRIPT_CONTINUE;
    }
}
