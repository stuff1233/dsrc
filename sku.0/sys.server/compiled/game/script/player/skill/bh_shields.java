package script.player.skill;

import script.*;
import script.library.buff;

public class bh_shields extends script.base_script {
    public int OnCreatureDamaged(obj_id self, obj_id attacker, obj_id weapon, int[] damage) {
        String shield_buff = "bh_shields";
        if (!buff.hasBuff(self, shield_buff)) {
            detachScript(self, "player.skill.bh_shields");
            return SCRIPT_CONTINUE;
        }
        int stackCount = (int)buff.getBuffStackCount(self, shield_buff);
        int timeLeft = (int)buff.getBuffTimeRemaining(self, shield_buff);
        if (stackCount < 2 || timeLeft < 2) {
            buff.applyBuff(self, "bh_shields_block");
            detachScript(self, "player.skill.bh_shields");
            return SCRIPT_CONTINUE;
        }
        buff.decrementBuffStack(self, shield_buff, 1);
        buff.applyBuff(self, "bh_shields_charged");
        return SCRIPT_CONTINUE;
    }
}
