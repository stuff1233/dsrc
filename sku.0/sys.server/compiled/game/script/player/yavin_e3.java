package script.player;

import script.*;

public class yavin_e3 extends script.base_script {
    public int OnCreatureDamaged(obj_id self, obj_id attacker, obj_id weapon, int[] damage) {
        setAttrib(self, HEALTH, getMaxAttrib(self, HEALTH));
        return SCRIPT_CONTINUE;
    }
    public int OnObjectDamaged(obj_id self, obj_id attacker, obj_id weapon, int damage) {
        setHitpoints(self, getMaxHitpoints(self));
        return SCRIPT_CONTINUE;
    }
    public int OnIncapacitated(obj_id self, obj_id killer) {
        return SCRIPT_OVERRIDE;
    }
}
