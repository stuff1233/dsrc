package script.e3demo;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.anims;
import script.library.space_utils;

public class e3_bridge_worker_2 extends script.base_script
{
    public e3_bridge_worker_2()
    {
    }
    public int OnAttach(obj_id self)
    {
        messageTo(self, "moveToLocationOne", null, 5, false);
        setObjVar(self, "intDestination", 1);
        return SCRIPT_CONTINUE;
    }
    public int moveToLocationOne(obj_id self, dictionary params)
    {
        pathToLocationOne(self);
        return SCRIPT_CONTINUE;
    }
    public void pathToLocationOne(obj_id self)
    {
        location destLoc = new location(getLocation(self));
        destLoc.setX(-5.9f);
        destLoc.setY(453f);
        destLoc.setZ(326.4f);
        setObjVar(self, "intIndex", 1);
        pathTo(self, destLoc);
    }
    public void pathToLocationTwo(obj_id self)
    {
        location destLoc = new location(getLocation(self));
        destLoc.setX(-6.8f);
        destLoc.setY(453f);
        destLoc.setZ(331.3f);
        setObjVar(self, "intIndex", 2);
        pathTo(self, destLoc);
    }
    public void pathToLocationThree(obj_id self)
    {
        location destLoc = new location(getLocation(self));
        destLoc.setX(-4.6f);
        destLoc.setY(206.5f);
        destLoc.setZ(307.4f);
        setObjVar(self, "intIndex", 3);
        pathTo(self, destLoc);
    }
    public void pathToLocationFour(obj_id self)
    {
        location destLoc = new location(getLocation(self));
        destLoc.setX(-0.4f);
        destLoc.setY(206.5f);
        destLoc.setZ(305.8f);
        setObjVar(self, "intIndex", 4);
        pathTo(self, destLoc);
    }
    public int OnMovePathComplete(obj_id self)
    {
        messageTo(self, "doFaceTo", null, 2.5f, false);
        return SCRIPT_CONTINUE;
    }
    public int doFaceTo(obj_id self, dictionary params)
    {
        int intIndex = getIntObjVar(self, "intIndex");
        location locTest = getLocation(self);
        if (intIndex == 1)
        {
            locTest.setX(-6.9f);
            locTest.setY(453f);
            locTest.setZ(325.5f);
        }
        else 
        {
            locTest.setX(-7.7f);
            locTest.setY(453f);
            locTest.setZ(331f);
        }
        faceTo(self, locTest);
        messageTo(self, "doTwiddle", null, 2.5f, false);
        return SCRIPT_CONTINUE;
    }
    public int doLocationOne(obj_id self, dictionary params)
    {
        pathToLocationOne(self);
        return SCRIPT_CONTINUE;
    }
    public int doLocationTwo(obj_id self, dictionary params)
    {
        pathToLocationTwo(self);
        return SCRIPT_CONTINUE;
    }
    public int doLocationThree(obj_id self, dictionary params)
    {
        pathToLocationThree(self);
        return SCRIPT_CONTINUE;
    }
    public int doLocationFour(obj_id self, dictionary params)
    {
        pathToLocationFour(self);
        return SCRIPT_CONTINUE;
    }
    public int doTwiddle(obj_id self, dictionary params)
    {
        float fltWait = rand(3, 8);
        doAnimationAction(self, "manipulate_medium");
        int intIndex = getIntObjVar(self, "intIndex");
        if (intIndex == 1)
        {
            messageTo(self, "doLocationTwo", null, fltWait, false);
        }
        else 
        {
            messageTo(self, "doLocationOne", null, fltWait, false);
        }
        return SCRIPT_CONTINUE;
    }
}
