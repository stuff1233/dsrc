package script.e3demo;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

public class hanger_elevator extends script.base_script
{
    public hanger_elevator()
    {
    }
    public int OnAttach(obj_id self)
    {
        setName(self, "an elevator terminal");
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi)
    {
        location myLoc = getLocation(player);
        obj_id building = getTopMostContainer(player);
        String currentCell = getCellName(myLoc.getCell());
        if (currentCell.equals("elevator_e3_down"))
        {
            playClientEffectObj(player, "clienteffect/elevator_rise.cef", player, null);
            obj_id objCell = getCellId(building, "elevator_e3_up");
            myLoc = new location();
            myLoc.setCell(objCell);
            myLoc.setX(19.3f);
            myLoc.setY(453.6f);
            myLoc.setZ(341.8f);
            setLocation(player, myLoc);
        }
        else 
        {
            playClientEffectObj(player, "clienteffect/elevator_descend.cef", player, null);
            obj_id objCell = getCellId(building, "elevator_e3_down");
            myLoc = new location();
            myLoc.setCell(objCell);
            myLoc.setX(-74.4f);
            myLoc.setY(204.9f);
            myLoc.setZ(330.2f);
            setLocation(player, myLoc);
        }
        return SCRIPT_CONTINUE;
    }
}
