package script.poi.exterior_spawner;


import script.obj_id;

public class exterior_spawner extends script.poi.interior_spawner.interior_spawner
{
	public int OnInitialize(obj_id self)
	{
		checkFactionalSpawners(self);
		spawnNpcs(self, null);
		return SCRIPT_CONTINUE;
	}
}
